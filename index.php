<?php
require_once 'Blocks/Block.php';
require_once 'Commands/Commands.php';
require_once 'Logic/Helper.php';
$commands=Commands::getInstance();

if (isset ($_GET['cmd'])) $cmd=$_GET['cmd'];
else $cmd='';

$data = array();
foreach ($_GET as $name => $value){
    $data[$name]=$value;
//    echo $name.'=>'.$value;
}

switch ($cmd) {
    case 'article':
        $commands->executeCommand('article', $data);
        break;
    case 'list':
        $commands->executeCommand('list', $data);
        break;
    case 'about':
        $commands->executeCommand('list', $data);
        break;
    default:
        $commands->executeCommand('list', NULL);
        break;
}
?>
<!DOCTYPE HTML>
<html class="no-js">
<head>
<?php 
switch ($cmd) {
    case 'article':
        $commands->render('article', 'title_description');
        break;
    case 'list':
        $commands->render('list', 'title_description');
        break;
    case 'about':
        $commands->render('list', 'title_description');
        break;
    default:
        $commands->render('list', 'title_description');
        break;
}
?>
<link rel="shortcut icon" href="/favicon.ico" type="image/x-ico"/>
<meta charset="utf-8">
<meta name="google-site-verification" content="4f-8fJGFZDgAhWIQ97Jk8hKj8bYPsTcM5eZBccZ0MHI" />
<meta name="bitly-verification" content="0f4e35dbfdff"/>
<meta property="fb:app_id" content="400534249971729"/>
<meta name="viewport" content="width=device-width, user-scalable=no" />
<link href="<?php echo Helper::getServerName() ?>/css/style.css" rel="stylesheet" type="text/css" >
<link rel="stylesheet" type="text/css" href="<?php echo Helper::getServerName() ?>/js/plugin_files/grid-accordion.css" media="screen"/>
<link rel="stylesheet" href="<?php echo Helper::getServerName() ?>/css/jquery.lightbox-0.5.css" type="text/css" media="screen" />
<link rel="stylesheet" href="<?php echo Helper::getServerName() ?>/css/carousel.css" type="text/css" media="screen" />
<script src="<?php echo Helper::getServerName() ?>/js/modernizr.custom.shiftedapps.js"></script>
<!-- analytics -->
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-29331355-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
</head>
<body>
<div class="wrapper">
	<!--<div class="statusBar">
    </div>-->
<div class="container">
    <!-- start header-->
    <?php
    Block::renderBlock(Helper::getServerName()."/FrontEndUsers/header.php", array('category_id'=>  Helper::getCategoryId())) ?>
    <!-- end header-->
    
    <!-- start featured or title-->
    <?php   
    //bad code CALIN!! Needs refactoring!
    switch ($cmd){
        case 'article':
            $commands->render('article', 'title_history');
            break;
        case 'list':
            $commands->render('list', 'featured');
            break;
        case 'about':
            break;
        default:
            $commands->render('list', 'featured');
            break;
    }
    ?>
    <!-- end featured or title-->
    
    <!--start Widgets -->
    
    <aside id="sidebar_right">
        <div id="wrapper_social_media">
        <div id="social_media">
    	<span class="mail"><a href="mailto:mail@shiftedapps.com"></a></span>
        <span class="twitter_sm"><a href="https://twitter.com/ShiftedApps"></a></span>
        <span class="facebook"><a href="https://www.facebook.com/ShiftedApps"></a></span>
        <span class="googleplus"><a href="https://plus.google.com/104728936089671382092/posts"></a></span>
        </div>
    <!-- end .social_media --> </div>
        
    <?php
        $commands->executeCommand('widgets', NULL);
    ?>
    </aside>
    <!-- end Widgets -->
    
    <!-- start content-->
    <div class="content"> 
    <?
    switch ($cmd){
        case 'article':
            $commands->render($cmd, 'article_block');
            break;
        case 'list':
            $commands->render($cmd, 'articles');
            break;
        case 'about':
            $commands->render('list', 'articles');
            break;
        default:
            $commands->render('list', 'articles');
            break;
    }
    ?>
    </div>
    <!-- end Content -->
   
    <!-- end .container --></div>
  <!-- end .wrapper --></div>
    
     <?php Block::renderBlock(Helper::getServerName()."/FrontEndUsers/footer.php", NULL)?>
    <script type="text/javascript" src="<?php echo Helper::getServerName() ?>/js/swipe.min.js"></script>
    <script type="text/javascript">
        window.slide = new Swipe(document.getElementById('slider'), {
	startSlide: 0,
	speed: 400,
        auto: 3000
});
        </script>
    <script type="text/javascript" src="http://twitter.com/javascripts/blogger.js"></script> 
    <script type="text/javascript" src="http://twitter.com/statuses/user_timeline/shiftedapps.json?callback=twitterCallback2&amp;count=5"></script>
    
</script>

</body>
</html>
