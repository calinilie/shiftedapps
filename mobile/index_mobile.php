<?php

?>
<!DOCTYPE HTML>
<html>
<head>
<title>ShiftedApps - A world of Apps</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="viewport" content="width=device-width, user-scalable=no" />
<link href="mCSS/style_mobile.css" rel="stylesheet" type="text/css">
</head>

<body>
<div class="wrapper">
	<!--<div class="statusBar">
    </div>-->
<div class="container">
  <div class="header">
  	<div class="logo">
  		<a href="#">
        <img alt="ShiftedApps Logo" id="mobile_logo" src="mCSS/graphics/mobile_logo.png" /></a>
        <!-- end .logo --></div>
        <div class="clearfloat"></div>
        <div class="navigation">

            <div id="nav1"><a href="#">Main</a></div>
            <div id="nav2"><a href="#">Link two</a></div>
            <div id="nav3"><a href="#">Link three</a></div>
            <div id="nav4"><a href="#">Link four</a></div>
            <span class="clearfloat"></span>
    <!--end of .navigation--></div>
    <div class="headline">
        <div id="mobileSlider">
<div id='slider' class='swipe'>
  <ul>
    <li style='display:block'><div>1</div></li>
    <li style='display:none'><div>2</div></li>
    <li style='display:none'><div>3</div></li>
    <li style='display:none'><div>4</div></li>
    <li style='display:none'><div>5</div></li>
    <li style='display:none'><div>6</div></li>
  </ul>
</div>
<a href='#' onclick='slide.prev();return false;'>prev</a> 
<a href='#' onclick='slide.next();return false;'>next</a>
</div>
    <!--end of .headline--></div>
    <!-- end .header --></div>
  
  <div class="content">
  	<article class="post">
    	<header>
        	<h1 class="postTitle">
        		Lorem ipsum dolor sit amet.
        	</h1>
            <p class="postMeta">
            	<time datetime="2011-12-01" class="post-date" pubdate>
                	By <a href="#">
                    	Name Surname
                       </a> 
                       | 29 Nov, 2011 at 15:00
                </time> 
                &bull; 
                <span class="postCategory"> 
                
                </span>
            </p>
        </header>
        
        <figure class="postImage">
        	<a href="#">
            	<img class="postedImage" alt="postImage" src="mCSS/post/test.png" />
       		</a>
        </figure>
        
        <div class="postContent">
        	<p>
            Lorem ipsum dolor sit amet, consectetur adipLorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam nisi orci, tempor a convallis quis, congue sed libero. Etiam sit amet lacus lorem. Mauris pretium purus vitae urna adipiscing mattis. Duis lacus massa, malesuada sit amet accumsan vitae, blandit vel neque. Praesent dolor lectus, mattis non iaculis eget, congue nec mauris. Ut scelerisque varius felis, eu semper metus iaculis in. 
Lorem ipsum dolor sit amet, consectetur adipLorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam nisi orci, tempor a convallis quis, congue sed libero. Etiam sit amet lacus lorem. Mauris pretium purus vitae urna adipiscing mattis. Duis lacus massa, malesuada sit amet accumsan vitae, blandit vel neque. Praesent dolor lectus, mattis non iaculis eget, congue nec mauris. Ut scelerisque varius felis, eu semper metus iaculis in.
            </p>
        </div>

        <span class="clearfloat"></span>
</article>

<article class="post">
    	<header>
        	<h1 class="postTitle">
        		Lorem ipsum dolor sit amet.
        	</h1>
            <p class="postMeta">
            	<time datetime="2011-12-01" class="post-date" pubdate>
                	By <a href="#">
                    	Name Surname
                       </a> 
                       | 29 Nov, 2011 at 15:00
                </time> 
                &bull; 
                <span class="postCategory"> 
                
                </span>
            </p>
        </header>
        
        <figure class="postImage">
        	<a href="#">
            	<img class="postedImage" alt="postImage" src="mCSS/post/test.png" />
       		</a>
        </figure>
        
        <div class="postContent">
        	<p>
            Lorem ipsum dolor sit amet, consectetur adipLorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam nisi orci, tempor a convallis quis, congue sed libero. Etiam sit amet lacus lorem. Mauris pretium purus vitae urna adipiscing mattis. Duis lacus massa, malesuada sit amet accumsan vitae, blandit vel neque. Praesent dolor lectus, mattis non iaculis eget, congue nec mauris. Ut scelerisque varius felis, eu semper metus iaculis in. 
Lorem ipsum dolor sit amet, consectetur adipLorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam nisi orci, tempor a convallis quis, congue sed libero. Etiam sit amet lacus lorem. Mauris pretium purus vitae urna adipiscing mattis. Duis lacus massa, malesuada sit amet accumsan vitae, blandit vel neque. Praesent dolor lectus, mattis non iaculis eget, congue nec mauris. Ut scelerisque varius felis, eu semper metus iaculis in.
            </p>
        </div>
        
        <span class="clearfloat"></span>
</article>

    	
 <!-- end .content --></div>
  
  
  <div class="footera">

    <!-- end .footer --></div>
  <!-- end .container --></div>
  <!-- end .wrapper --></div>
  	<div class="footer">
  		<div class="shadow_bar">
   	 	</div>
    	<div class="footer_content">
        	<div class="footer_left">
                 <br />
                <ul class="nav_footer">
                    <li><a href="#">Home</a></li>
                    <li><a href="#">Android apps review</a></li>
                    <li><a href="#">iOS apps review</a></li>
                    <li><a href="#">Posts</a></li>
                    <li><a href="#">About</a></li>
                    <li><a href="#">Our work</a></li>
                    <li><a href="#">Contact</a></li>
                    <li><a href="#">Suggestions</a></li>
                </ul>
            	
            </div>
            
            <div class="footer_middle">
            	<div class="footer_logo">
                	<a href=""><img alt="ShiftedApps Logo Footer" src="mCSS/graphics/mobile_logoB.png" /></a>
                </div>
                
            </div>
            
            <div class="footer_right">
                <div id="mail_stamp">
                	<a></a>
                </div>
                <div id="twitter_stamp">
                	<a></a>
                </div>
                <div id="facebook_stamp">
                	<a></a>
                </div>
                <div id="googleplus_stamp">
                	<a></a>
                </div>
            </div>
            
            <div class="clearfloat">
            </div>
            <!--<nav class="nav_footer">© 2012 Shiftedapps.com  All rights reserved.     Main |  Android app reviews  |  iOS app reviews  |  About  |  Contact  |  Suggestions
			</nav>-->
            
            
        
    	<!-- end .footer_content --></div>
    <!-- end .footer --></div>
 <script type="text/javascript" src="<?php echo Helper::getServerName() ?>/js/swipe.min.js"></script>  
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.js"></script>
<script type="text/javascript">
    $(document).ready(function () {  
      var top = $('#social_media').offset().top - parseFloat($('#social_media').css('marginTop').replace(/auto/, 0));
      $(window).scroll(function (event) {
        // what the y position of the scroll is
        var y = $(this).scrollTop();
      
        // whether that's below the form
        if (y >= top) {
          // if so, ad the fixed class
          $('#social_media').addClass('fixed');
        } else {
          // otherwise remove it
          $('#social_media').removeClass('fixed');
        }
      });
    });
    
    window.slide = new Swipe(document.getElementById('slider'), {
	startSlide: 0,
	speed: 400,
        auto: 3000
});
</script>
</body>
</html>
