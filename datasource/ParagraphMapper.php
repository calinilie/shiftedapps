<?php
$path=dirname(dirname(__FILE__));
require_once $path.'/model/Paragraph.php';
require_once $path.'/model/ParagraphDTO.php';


class ParagraphMapper{
    
    private static $instance;

    private function __construct(){
        
    }
    
    public static function getIntsance(){
        if (!isset (self::$instance))
            self::$instance=new ParagraphMapper();
        return self::$instance;
    }
    
    public function getParagraphsForArticle($id){
        $paragraphs=array();
        $id=mysql_real_escape_string($id);
        $query="select paragraph_id, text, added from paragraphs where article_id =".$id. " order by added desc";
        $result = mysql_query($query);
        while ($row=  mysql_fetch_array($result)){
            $paragraph_id=$row['paragraph_id'];
            $p=new Paragraph($paragraph_id, $row['text'], $row['added']);
            $paragraphs[$paragraph_id]=$p;
        }
        return $paragraphs;
    }
    
    public function getLatestParagraphForArticle($id){
        $paragraph=NULL;
        $id=mysql_real_escape_string($id);
        $query="select paragraph_id, text, max(added) as added from paragraphs where article_id = ".$id;
        $result=mysql_query($query);
        while($row = mysql_fetch_array($result)){
            $paragraph=new Paragraph($row['paragraph_id'], $row['text'], $row['added']);
        }
        return $paragraph;
    }
    
    public function getLatestParagraphsDTO($limit){
        $updates=array();
        $limit=mysql_real_escape_string($limit);
        $query="SELECT a.title as article_title, p.added as added, a.url_identifier as url_identifier, p.text as text 
                FROM articles a, paragraphs p, categories_articles ca 
                WHERE a.article_id = p.article_id 
                and a.active=1
                and a.article_id=ca.article_id 
                and ca.category_id!=3 
                ORDER BY p.added DESC 
                LIMIT 0 , ".$limit;
        $result=mysql_query($query) or die(mysql_error());
        while ($row = mysql_fetch_array($result)){
            $update=new ParagraphDTO($row['article_title'], $row['url_identifier'], $row['added'], $row['text']);
            array_push($updates, $update);
        }
        return $updates;
    }
    
    public function editParagraph($paragraph){
//        $text=mysql_real_escape_string($paragraph->getText());
        $text=$paragraph->getText();
        $id=mysql_real_escape_string($paragraph->getId());
        $query="update paragraphs set text = '".$text."' where paragraph_id=".$id;
        $result=mysql_query($query) or die(mysql_error());
        return $result;
    }
    
    public function addParagraph($article_id, $paragraph){
        $text=$paragraph->getText();
        $article_id=mysql_real_escape_string($article_id);
        $query="insert into paragraphs values(null, ".$article_id.", '".$text."', CURRENT_TIMESTAMP)";
        $result=mysql_query($query) or die(mysql_error());
        return $result;
    }
    
    public function deleteParagraph($paragraph) {
        $paragraph_id=mysql_real_escape_string($paragraph->getId());
        $query="delete from paragraphs where paragraph_id=".$paragraph_id;
        $result=mysql_query($query) or die(mysql_error());
        return $result;
    }
}
?>
