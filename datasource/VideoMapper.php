<?php
$path=dirname(dirname(__FILE__));
require_once $path.'/model/Video.php';

class VideoMapper{
    
    private static $instance;
    
    private function __construct() {
        
    }
    
    public static function getInstance(){
        if (!isset (self::$instance))
            self::$instance=new VideoMapper();
        return self::$instance;
    }
    
    public function getVideoForArticle($id){
        $video=null;
        $id=mysql_real_escape_string($id);
        $query = "select vid_id as id, link, comments from vids where article_id = ".$id;
        $result=mysql_query($query) or die(mysql_error());
        while ($row = mysql_fetch_array($result)){
            $video=new Video($row['id'], $row['link'], $row['comments']);
        }
        return $video;
    }
    
    public function updateVideo($article_id, $video){
        $video_id=$video->getId();
        $link=$video->getLink();
        $comments=$video->getComments();
        if ($video_id!=0){
            $query="update vids 
                    set link='".$link."', 
                    comments='".$comments."' 
                    where vid_id=".$video_id;
        }
        else{
            $query="insert into vids values(NULL, ".$article_id.",'".$link."', '".$comments."')";
        }
        echo $query.'<br />';
        $result=mysql_query($query) or die(mysql_error());
        return $result;
    }
}
?>
