<?php
$path=dirname(dirname(__FILE__));
require_once $path.'/datasource/ArticleMapper.php';
require_once $path.'/datasource/ParagraphMapper.php';
require_once $path.'/datasource/CategoryMapper.php';

class FacadeUser{
    
    private static $instance;
    /**
     *
     * @var ArticleMapper
     */
    private $articleMapper;
    private $paragraphMapper;
    private $picMapper;
    private $videoMapper;
    private $categoryMapper;
    
    private function __construct() {
        $this->articleMapper=ArticleMapper::getInstance();
        $this->paragraphMapper=ParagraphMapper::getIntsance();
        $this->picMapper=PicMapper::getInstance();
        $this->videoMapper=VideoMapper::getInstance();
        $this->categoryMapper=new CategoryMapper();
    }

    
    public static function getInstance(){
        if (!isset (self::$instance)){
            self::$instance=new FacadeUser();
        }
        return self::$instance;
    }
    
    
    private function connect(){
        $hostname='shiftedapps.db.8865381.hostedresource.com';
        mysql_connect($hostname, "shiftedappsfe", "FRuTRadUDum62h") or die(mysql_error());
        mysql_select_db("shiftedapps");
    }
    
    private function disconnect(){
        mysql_close();
    }
    
    public function getFullArticles($category, $limit){
        $this->connect();
        $articles=$this->articleMapper->getFullArticles($category, $limit);
        $this->disconnect();
        return $articles;
    }
    
    /**
     *
     * @param type $category if == 0, then category filter is skipped
     * @param type $limit default is 7
     * @param type $featured if == 0, then featured filter is skipped
     * @return type a collection of articleDTO based on selected filters
     */
    public function getArticlesDTO($category, $limit, $featured){
        $this->connect();
        $articlesDTO=$this->articleMapper->getArticlesDTO($category, $limit, $featured);
        $this->disconnect();
        return $articlesDTO;
    }
    
    public function getParagraphsForArticle($id){
        $this->connect();
        $paragraphs=$this->paragraphMapper->getParagraphsForArticle($id);
        $this->disconnect();
        return $paragraphs;
    }
    
    public function getFullArticle($url_identifier){
        $this->connect();
        $article=$this->articleMapper->getFullArticle($url_identifier);
        $this->disconnect();
        return $article;
    }
    
    public function getUpdates($limit){
        $this->connect();
        $updates=$this->paragraphMapper->getLatestParagraphsDTO($limit);
        $this->disconnect();
        return $updates;
    }
    
    public function getCategory($urlIdentifier){
        $this->connect();
        $category=$this->categoryMapper->getCategory($urlIdentifier);
        $this->disconnect();
        return $category;
    }
    
    public function loadMoreArticles($category, $lowerLimit, $limit){
        $this->connect();
        $articles=$this->articleMapper->loadMoreArticles($category, $lowerLimit, $limit);
        $this->disconnect();
        return $articles;
    }
}
?>
