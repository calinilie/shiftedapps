<?php
$path=dirname(dirname(__FILE__));
require_once $path.'/model/Pic.php';

class PicMapper{
    
    private static $instance;
    
    private function __construct() {
        
    }
    
    public static function getInstance(){
        if (!isset (self::$instance))
            self::$instance=new PicMapper();
        return self::$instance;
    }
    /**
     *
     * @param type $id integer
     * @param type $withThumbnail boolean -> by default true, set to false wo fetch all images minus the thumbnail image
     * @return Pic 
     */
    public function getPicsForArticle($id, $withThumbnail=true){
        $pics= array();
        $id=mysql_real_escape_string($id);   
        //assign query for default behaviour ->fetch all including thumbnail
        $query="select pic_id as id, path, comments, added, thumbnail from pics where article_id=".$id." order by added desc";
        //change query is no thumbnail is requested!
        if ($withThumbnail==false) $query="select pic_id as id, path, comments, added, thumbnail from pics where article_id=".$id." and thumbnail=0 order by added desc";
        $result=mysql_query($query);
        while ($row = mysql_fetch_array($result)){
            $picture_id=$row['id'];
            $pic = new Pic($picture_id, $row['path'], $row['comments'], $row['added']);
            $pic->setThumbnail($row['thumbnail']);
            $pics[$picture_id]=$pic;
        }
        return $pics;
    }
    
    public function getThumbnailForArticle($id){
        $pic=null;
        $id=mysql_real_escape_string($id);
        $query="select pic_id as id, path, comments, added from pics where article_id=".$id." and thumbnail = 1";
        $result=mysql_query($query) or die(mysql_error());
        while ($row = mysql_fetch_array($result)){
            $pic=new Pic($row['id'], $row['path'], $row['comments'], $row['added']);
        }
        return $pic;
    }
    
    public function insertPicture($article_id, $picture){
        $path=$picture->getPath();
        $thumbnail=mysql_real_escape_string($picture->getThumbnail());
        $comments=mysql_real_escape_string($picture->getComments());
        $query="insert into pics(pic_id, article_id, path, comments, added, thumbnail) 
                values(null, ".$article_id.", '".$path."', '".$comments."', CURRENT_TIMESTAMP, ".$thumbnail.")";
        $result=mysql_query($query) or die(mysql_error());
        return $result;
    }
    
    public function deletePicture($picture){
        $id=$picture->getId();
        $query="delete from pics where pic_id=".$id;
        $result=mysql_query($query) or die(mysql_error());
        return $result;
    }
    
    public function updatePicture($picture){
        $id=mysql_real_escape_string($picture->getId());
        $comments=mysql_real_escape_string($picture->getComments());
        $thumbanil=mysql_real_escape_string($picture->getThumbnail());
        $query="update pics set comments='".$comments."', thumbnail=".$thumbanil." where pic_id=".$id;
        $result=mysql_query($query) or die(mysql_error());
        return $result;
    }
    
    public function resetThumbnails($article_id){
        $article_id=mysql_real_escape_string($article_id);
        $query="update pics set thumbnail = 0 where article_id =".$article_id;
        $result=mysql_query($query) or die(mysql_error());
        return $result;
    }
}
?>
