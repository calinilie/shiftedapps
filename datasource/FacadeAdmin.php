<?php
$path=dirname(dirname(__FILE__));
require_once $path.'/datasource/ArticleMapper.php';
require_once $path.'/datasource/ParagraphMapper.php';
require_once $path.'/datasource/PicMapper.php';
require_once $path.'/datasource/VideoMapper.php';

class FacadeAdmin{
    private static $instance;
    /**
     *
     * @var ArticleMapper 
     */
    private $articleMapper;
    private $paragraphMapper;
    private $picMapper;
    private $videoMapper;
    
    private function __construct() {
        $this->articleMapper=ArticleMapper::getInstance();
        $this->paragraphMapper=ParagraphMapper::getIntsance();
        $this->picMapper=PicMapper::getInstance();
        $this->videoMapper=VideoMapper::getInstance();
    }
    
    public static function getInstance(){
        if (!isset (self::$instance)){
            self::$instance=new FacadeAdmin();
        }
        return self::$instance;
    }
    
    public function connect(){
        $hostname='shiftedapps.db.8865381.hostedresource.com';
        $link=mysql_connect($hostname, "shiftedapps", "P43sP6WrEdR6wA") or die(mysql_error());
        mysql_select_db("shiftedapps");
        return $link;
    }
    
    public function disconnect(){
        mysql_close();
    }
    
    public function getFullArticleById($id){
        $this->connect();
        $article=$this->articleMapper->getFullArticleById($id);
        $this->disconnect();
        return $article;
    }
    
    public function deleteParagraph($paragraph){
        return $this->paragraphMapper->deleteParagraph($paragraph);
    }
    
    public function updateParagraph($paragraph){
        return $this->paragraphMapper->editParagraph($paragraph);
    }
    
    public function addParagraph($article_id, $paragraph){
        return $this->paragraphMapper->addParagraph($article_id, $paragraph);
    }
    
    public function addArticle($article){
        return $this->articleMapper->insertArticle($article);
    }
    
    public function updateArticle($article){
        return $this->articleMapper->updateArticle($article);
    }
    
    public function addPicture($article_id, $picture){
        return $this->picMapper->insertPicture($article_id, $picture);
    }
    
    public function deletePicture($picture){
        return $this->picMapper->deletePicture($picture);
    }
    
    public function resetThumbnails($article_id){
        return $this->picMapper->resetThumbnails($article_id);
    }
    
    public function updatePicture($picture){
        return $this->picMapper->updatePicture($picture);
    }
    
    public function updateVideo($article_id, $video){
        return $this->videoMapper->updateVideo($article_id, $video);
    }
    
    public function autocommitFalse(){
        mysql_query("set autocommit=0");
    }
    
    public function autocommitTrue(){
        mysql_query("set autocommit=1");
    }
    
    public function commit(){
        mysql_query("commit");
    }
    
    public function rollback(){
        mysql_query("rollback");
    }
    
    public function deleteArticle($url){
        $this->connect();
        $result=$this->articleMapper->deleteArticle($url);
        $this->disconnect();
        return $result;
    }
}
?>
