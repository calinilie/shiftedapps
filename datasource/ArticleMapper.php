<?php
$path=dirname(dirname(__FILE__));
require_once $path.'/model/Article.php';
require_once $path.'/model/ArticleDTO.php';
require_once $path.'/datasource/ParagraphMapper.php';
require_once $path.'/datasource/PicMapper.php';
require_once $path.'/datasource/VideoMapper.php';
require_once $path.'/datasource/CategoryMapper.php';

class ArticleMapper {
    
    private static $instance;
    /**
     * @var ParagraphMapper
     */
    private $paragraphMapper;
    /**
     * @var PicMapper
     */
    private $picMapper;
    /**
     * @var VideoMapper
     */
    private $videoMapper;
    /**
     * @var CategoryMapper
     */
    private $categoryMapper;
    
    private function __construct() {
        $this->paragraphMapper=ParagraphMapper::getIntsance();
        $this->videoMapper=VideoMapper::getInstance();
        $this->picMapper=PicMapper::getInstance();
        $this->categoryMapper=new CategoryMapper();
    } 
    
    public static function getInstance(){
        if (!isset (self::$instance))
                self::$instance=new ArticleMapper();
       return self::$instance;
    }
   
    public function getFullArticles($category, $limit){         
         if ($category==0) $articles=$this->getAllArticles($limit);
         else $articles=$this->getArticles($catgeory, $limit);
         
         $result=array();
         foreach ($articles as $article){
            //get paragraphs from DB
            $paragraphs = $this->paragraphMapper->getParagraphsForArticle($article->getId());
            $article->setParagraphs($paragraphs);
            
            //get pics from DB
            $pics=$this->picMapper->getPicsForArticle($article->getId(), false);
            $article->setPics($pics);
            //$thumbnail
            $thumbnail=$this->picMapper->getThumbnailForArticle($article->getId());
            if (isset($thumbnail)) $article->setThumbnail($thumbnail);
            
            //video
            $video=$this->videoMapper->getVideoForArticle($article->getId());
            if (isset ($video)) $article->setVideo ($video);
            
            array_push($result, $article);
         }
         return $result;
    }
    
    public function getFullArticle($url_identifier){
        $article=null;
        $paragraphMapper=ParagraphMapper::getIntsance();
        $picMapper=PicMapper::getInstance();
        $videoMapper=VideoMapper::getInstance();
        
        $url_identifier=mysql_real_escape_string($url_identifier);
        $query="select a.article_id, title, DATE_FORMAT( added,  '%M %D, %Y' ) AS added, url_identifier, category_id, description 
                from articles a, categories_articles ca
                where url_identifier like '".$url_identifier."' 
                and a.article_id=ca.article_id";
        $result=mysql_query($query) or die(mysql_error()); 
        while ($row =  mysql_fetch_array($result)){
            //get article
            $article=new Article($row['article_id'], $row['title'], $row['added'], $row['url_identifier']);
            $category_id=$row['category_id'];
            $category=$this->categoryMapper->getCategoryById($category_id);
            $article->setCategory($category);
            $article->setDescription($row['description']);
            
            //get paragraphs from DB
            $paragraphs = $paragraphMapper->getParagraphsForArticle($article->getId());
            $article->setParagraphs($paragraphs);
            
            //get pics from DB
            $pics=$picMapper->getPicsForArticle($article->getId(), false);
            $article->setPics($pics);
            
            //$thumbnail
            $thumbnail=$picMapper->getThumbnailForArticle($article->getId());
            if (isset($thumbnail)) $article->setThumbnail($thumbnail);
            
            //video
            $video=$videoMapper->getVideoForArticle($article->getId());
            if (isset ($video)) $article->setVideo($video);
        }
        return $article; 
    }
    
    public function getFullArticleById($id){
        $article=null;
        $paragraphMapper=ParagraphMapper::getIntsance();
        $picMapper=PicMapper::getInstance();
        $videoMapper=VideoMapper::getInstance();
        
        $url_identifier=mysql_real_escape_string($id);
        $query="select a.article_id, title, added, url_identifier, category_id, featured, active, description 
                from articles a, categories_articles ca
                where a.article_id = '".$id."' 
                and a.article_id=ca.article_id";
        $result=mysql_query($query) or die(mysql_error()); 
        while ($row =  mysql_fetch_array($result)){
            //get article
            $article=new Article($row['article_id'], $row['title'], $row['added'], $row['url_identifier']);
            $article->setCategoryId($row['category_id']);
            $article->setFeatured($row['featured']);
            $article->setActive($row['active']);
            $article->setDescription($row['description']);
            //get paragraphs from DB
            $paragraphs = $paragraphMapper->getParagraphsForArticle($article->getId());
            $article->setParagraphs($paragraphs);
            
            //get pics from DB
            $pics=$picMapper->getPicsForArticle($article->getId());
            $article->setPics($pics);
            
            //$thumbnail
            $thumbnail=$picMapper->getThumbnailForArticle($article->getId());
            if (isset($thumbnail)) $article->setThumbnail($thumbnail);
            
            //video
            $video=$videoMapper->getVideoForArticle($article->getId());
            if (isset ($video)) $article->setVideo ($video);
        }
        return $article; 
    }
    
    public function getArticlesDTO($category, $limit, $featured){
        $paragraphMapper=ParagraphMapper::getIntsance();
        $pictureMapper=PicMapper::getInstance();
        
        $DTOArticles=array();
        
        if ($category==-1){//bad workaround for fetching incative articles for the admin part, consider refactoring architecture
            $articles=$this->getAllArticlesForAdmin($limit);
        }
        else{
            if ($featured==0){
                if ($category==0) $articles=$this->getAllArticles(0, $limit);
                else $articles=$this->getArticles($category, 0, $limit);
            }
            else {
                if ($category==0) $articles=$this->getFeaturedArticles($limit);
                else $articles=$this->getFeaturedArticlesByCategory ($category, $limit);
            }
        }
        
        foreach($articles as $article){
            $id=$article->getId();
            $title=$article->getTitle();
            $added=$article->getAdded();
            $url_identifier=$article->getUrlIdentifier();
            $thumbnail=$pictureMapper->getThumbnailForArticle($article->getId());
            $latestParagraph=$paragraphMapper->getLatestParagraphForArticle($article->getId());
            
            $DTOArticle= new ArticleDTO($id, $title, $latestParagraph, $thumbnail, $added, $url_identifier);
            
            array_push($DTOArticles, $DTOArticle);
        }
        return $DTOArticles;
    }
    
    private function getArticles($category, $lowerLimit, $limit){
        $category=mysql_real_escape_string($category);
        $limit=mysql_real_escape_string($limit);
        $lowerLimit=mysql_real_escape_string($lowerLimit);
        $articles=array();
        $query="SELECT a.article_id, title, url_identifier, DATE_FORMAT( added,  '%W, %D of %M %Y at %H:%i' ) AS posted
                FROM articles a, categories_articles ca
                WHERE category_id = ".$category." 
                AND active =1
                AND a.article_id = ca.article_id
                ORDER BY added DESC   
                limit ".$lowerLimit.", ".$limit;   
        $result = mysql_query($query) or die(mysql_error()); 
        while ($row = mysql_fetch_array($result)){
            $article=new Article($row['article_id'], $row['title'], $row['posted'], $row['url_identifier']);
            array_push($articles, $article);
        }
        return $articles;
    }
    
    private function getAllArticles($lowerLimit, $limit){
        $limit=mysql_real_escape_string($limit);
        $lowerLimit=mysql_real_escape_string($lowerLimit);
        $articles=array();
       $query="SELECT a.article_id, a.title, a.url_identifier, date_format(a.added, '%W, %D of %M %Y at %H:%i') as posted 
                FROM  articles a, categories_articles ca
                where active=1 
                and a.article_id=ca.article_id
                and ca.category_id!=3 
                order by added desc 
                limit ".$lowerLimit.", ".$limit;
        $result = mysql_query($query) or die(mysql_error());  
        while ($row = mysql_fetch_array($result)){
            $article=new Article($row['article_id'], $row['title'], $row['posted'], $row['url_identifier']);
            array_push($articles, $article);
        }
        return $articles;
    }
    
    private function getAllArticlesForAdmin($limit){
        $limit=mysql_real_escape_string($limit);
        $articles=array();
        $query='SELECT * 
                FROM  articles
                order by added desc 
                limit 0, '.$limit;        
        $result = mysql_query($query) or die(mysql_error());  
        while ($row = mysql_fetch_array($result)){
            $article=new Article($row['article_id'], $row['title'], $row['added'], $row['url_identifier']);
            array_push($articles, $article);
        }
        return $articles;
    }
        
    private function getFeaturedArticles($limit){
        $limit=mysql_real_escape_string($limit);
        $articles=array();
        $query="select a.article_id, a.title, a.added, a.url_identifier  
                from articles a, categories_articles ca 
                where featured=1 
                    and active=1 
                    and a.article_id=ca.article_id 
                    and ca.category_id!=3 
                order by added desc 
                limit 0, ".$limit;
        $result=mysql_query($query) or die(mysql_error());
        while ($row=  mysql_fetch_array($result)){
            $article=new Article($row['article_id'], $row['title'], $row['added'], $row['url_identifier']);
            array_push($articles, $article);
        }
        return $articles;
    }
    
    private function getFeaturedArticlesByCategory($category, $limit){
        $limit=mysql_real_escape_string($limit);
        $category=mysql_real_escape_string($category);
        $articles=array();
        $query="SELECT a.article_id, title, url_identifier, DATE_FORMAT( added,  '%W, %D of %M %Y at %H:%i' ) AS posted
                FROM articles a, categories_articles ca
                WHERE category_id = ".$category." 
                AND active =1
                AND featured =1
                AND a.article_id = ca.article_id
                ORDER BY added DESC 
                limit 0, ".$limit;
        $result=mysql_query($query) or die(mysql_error());
        while ($row=  mysql_fetch_array($result)){
            $article=new Article($row['article_id'], $row['title'], $row['posted'], $row['url_identifier']);
            array_push($articles, $article);
        }
        return $articles;
    }
    
    public function insertArticle($article){
        $title=$article->getTitle();
        $category=$article->getCategoryId();
        $featured=$article->getFeatured();
        $url_identifier=$article->getUrlIdentifier();
        $active=$article->getActive();
        $description=$article->getDescription();
        $query="INSERT INTO articles values (NULL, '".$title."', CURRENT_TIMESTAMP, ".$featured.", '".$url_identifier."', ".$active.", '".$description."')";
//        echo PHP_EOL.PHP_EOL.$query;
        $result=mysql_query($query) or die(mysql_error());
        $id=mysql_insert_id();
        $reuslt=$this->insertArticleToCategory($id, $category)&&$result;
        $stuff=array("result"=>$result, "id"=>$id);
        return $stuff;
    }
    
    public function updateArticle($article){
        $article_id=mysql_real_escape_string($article->getId());
        $title=mysql_real_escape_string($article->getTitle());
        $featured=mysql_real_escape_string($article->getFeatured());
        $category=mysql_real_escape_string($article->getCategoryId());
        $description=mysql_real_escape_string($article->getDescription());
        $active=$article->getActive();
        $query="update articles set title='".$title."', featured=".$featured.", active=".$active.", description='".$description."' where article_id = ".$article_id;
        $result=mysql_query($query) or die(mysql_error());
        $result=$result&&$this->updateArticleToCategory($article);
        return $result;
    }
    
    private function insertArticleToCategory($article_id, $category_id){
        $query="insert into categories_articles values(".$article_id.",".$category_id.")";
//        echo PHP_EOL.PHP_EOL.$query;
        $result=mysql_query($query)or die(mysql_error());
        return $result;
    }
    /**
     * @param type $article Article
     */
    private function updateArticleToCategory($article){
        $category_id=$article->getCategoryId();
        $article_id=$article->getId();
        $query="update categories_articles 
                set category_id = ".$category_id." 
                where article_id = ".$article_id;
        echo PHP_EOL.PHP_EOL.$query;
        $result=mysql_query($query);
        return $result;
    }
    
    public function deleteArticle($url){
        $query="delete from articles where url_identifier like '".$url."'";
        mysql_query($query)or die(mysql_error());
    }
    
    public function loadMoreArticles($category, $lowerLimit, $limit){
        $DTOArticles=array();
        if ($category==0) $articles=$this->getAllArticles($lowerLimit, $limit);
        else $articles=$this->getArticles($category, $lowerLimit, $limit);
        
        foreach($articles as $article){
            $id=$article->getId();
            $title=$article->getTitle();
            $added=$article->getAdded();
            $url_identifier=$article->getUrlIdentifier();
            $thumbnail=$this->picMapper->getThumbnailForArticle($article->getId());
            $latestParagraph=$this->paragraphMapper->getLatestParagraphForArticle($article->getId());
            
            $DTOArticle= new ArticleDTO($id, $title, $latestParagraph, $thumbnail, $added, $url_identifier);
            
            array_push($DTOArticles, $DTOArticle);
        }
        return $DTOArticles;
    }
    
}
?>
