<?php
$path=dirname(dirname(__FILE__));
class UserMappper{
    
    private static $instance;
    private $dbh;
    
    private function __construct() {
        
    }
    
    public static function getInstance(){
        if (!isset (self::$instance))
                self::$instance=new UserMappper();
       return self::$instance;
    }
    
    private function connect(){
        $hostname='shiftedapps.db.8865381.hostedresource.com';
        $db_user="shiftedappsfe";
        $db_pass="FRuTRadUDum62h";
        $this->dbh = new PDO('mysql:host='.$hostname.';dbname=shiftedapps', $db_user, $db_pass);
    }
    
    private function disconnect(){
        $this->dbh=null;
    }

    public function adminAuth($user, $pass){
        $ok=false;
        $this->connect();
        $stmt=$this->dbh->prepare("select username, pass from credentials where username like ? and pass like ?");
        $stmt->bindParam(1, $user);
        $stmt->bindParam(2, $pass);
        if ($stmt->execute()){
            $result=$stmt->fetch();
            if (count($result)>1){
                if (isset ($result['username'])&&isset ($result['pass'])) $ok=TRUE;
            }
            else $ok=false;
        }  
        else $ok=false;
        $this->disconnect();
        return $ok;
    }
    
    //later!
    public function isConnected($time){
        $ok=true;
        $ip=$_SERVER['REMOTE_ADDR'];
        $this->connect();
        $stmt=$this->dbh->prepare("select user_id, max(time_stamp) as start_time, last_activity from logins where ip = ?");
        $stmt->bindParam(1, $ip);
        if ($stmt->execute()){
            if (count($result=$stmt->fetch())==0){
                $ok=false;
            }
            else{
                if ($time!=0){
                    $target=strtotime($result['last_activity'])+$time;
                    $now=strtotime('now');
                    if ($now>$target) $ok=false;
                    //set logged off
                }
            }
        }
        else $ok=false;
        $this->disconnect();
        return $ok;
    }
    
}
?>
