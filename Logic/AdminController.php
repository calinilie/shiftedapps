<?php
$path=dirname(dirname(__FILE__));
require_once $path.'/Logic/UnitOfWork.php';
require_once $path.'/datasource/UserMapper.php';
require_once $path.'/Logic/Helper.php';
class AdminController{
    
    private static $instance;
    /**
     * @var UnitOfWork
     */
    private $uow;
    
    private function __construct() {
        if (isset ($_SESSION['uow'])) $this->getUowFromMemory();
    }
    
    public static function getInstance(){
        if (!isset (self::$instance))
                self::$instance=new AdminController();
       return self::$instance;
    }
    
    private function getUowFromMemory(){
        $this->uow=Helper::getFromSession('uow');
    }

    private function saveUowToMemory(){
        Helper::saveToSession('uow', $this->uow);
    }
    
    public function isLoggedIn(){
        $logged=true;
        if (isset ($_SESSION['logged'])){
            $logged=$_SESSION['logged'];
            if (time() - $_SESSION['LAST_ACTIVITY'] > 3600) {
                session_destroy();
                session_unset();
                $logged=false;
            } else {
                $_SESSION['LAST_ACTIVITY'] = time();
            }
        }
        else $logged=FALSE;
        return $logged;
    }
    
    public function adminAuth($user, $pass){
        if (UserMappper::getInstance()->adminAuth($user, $pass)==TRUE){
            $_SESSION['logged']=true;
            $_SESSION['LAST_ACTIVITY']=time();
            return true;
        }
        return false;
    }
    
    public function logout(){
        $_SESSION['logged']=false;
    }
    
    public function startUpdate($article_id){
        $this->uow=UnitOfWork::getInstance();
        $this->uow->startUpdate($article_id);
        $this->saveUowToMemory();
    }
    
    public function startNew($article_title, $category, $featured, $active, $description){
        $this->uow=UnitOfWork::getInstance();
        $this->uow->startNew($article_title, $category, $featured, $active, $description);
        $this->saveUowToMemory();        
    }
    
    public function editParagraph($paragraph_id ,$text){
        $this->uow->editParagraph($paragraph_id ,$text);
        $this->saveUowToMemory();
    }
    
    public function addParagraph($text){
        $this->uow->addParagraph($text);
        $this->saveUowToMemory();
    }
    
    public function deleteParagraph($paragraph_id){
       $this->uow->deleteParagraph($paragraph_id);
       $this->saveUowToMemory();
    
    }
    
    public function updateArticle($title, $category){
        $this->uow->updateArticle($title, $category);
        $this->saveUowToMemory();
    }
    
    public function updateArticleFeatured($featured){
        $this->uow->updateArticleFeatured($featured);
        $this->saveUowToMemory();
    }
    
    public function updateArticleActive($active) {
        $this->uow->updateArticleActive($active);
        $this->saveUowToMemory();
    }
    
    public function updateArticleDescription($description){
        $this->uow->updateArticleDescription($description);
        $this->saveUowToMemory();
    }
    
    public function addPhoto($path, $comments, $thumbnail){
        $this->uow->addPhoto($path, $comments, $thumbnail);
        echo $thumbnail.PHP_EOL;
        $this->saveUowToMemory();
    }
    
    public function editPhoto($picture_id, $comments, $thumnail){
        $this->uow->editPhoto($picture_id, $comments, $thumnail);
        $this->saveUowToMemory();
    }
    
    public function deletePhoto($picture_id){
        $this->uow->deletePhoto($picture_id);
        $this->saveUowToMemory();
    }
    
    public function updateVideo($video_id, $link, $comments){
        $this->uow->updateVideo($video_id, $link, $comments);
        $this->saveUowToMemory();
    }
    
    public function save(){
        $this->getUowFromMemory();
        $this->uow->save();
        unset ($_SESSION['uow']);
    }
    
    public function getArticle(){
        $this->getUowFromMemory();
        return $this->uow->getArticle();
    }

}

?>
