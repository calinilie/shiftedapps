<?php
$path=dirname(dirname(__FILE__));
require_once $path.'/datasource/FacadeUser.php';


class UserController{
    private static $instance;
    private $article;
    
    private function __construct() {
        
    }
    
    public static function getInstance(){
        if (!isset (self::$instance))
                self::$instance=new UserController();
        return self::$instance;    
    }
    
    public function getArticle(){
        if (($this->article!=NULL)) echo $this->article->display();
        else {
            $this->article = FacadeUser::getInstance()->getFullArticle(1);
            echo $this->article->display();
            echo '<br />from DataBase';
        }
    }  
}

UserController::getInstance()->getArticle();
?>
