<?php
$path=dirname(dirname(__FILE__));
require_once $path.'/datasource/FacadeAdmin.php';

class UnitOfWork{
    private static $instance;
    /**
     * @var Article
     */
    private $article;
    private $dirty_paragraphs;
    private $new_paragraphs;
    private $delete_paragraphs;
    private $article_state;
    private $delete_photos;
    private $dirty_photos;
    private $new_photos;
    private $videos;
    private $adminFacade;

    private $unchanged_article=0;
    private $dirty_article=1;
    private $new_article=2;


    private function __construct(){
        $this->adminFacade=FacadeAdmin::getInstance();
    }
    
    public static function getInstance(){
        if (!isset (self::$instance))
                self::$instance=new UnitOfWork();
       return self::$instance;
    }
    
    private function init(){
        $this->delete_paragraphs=array();
        $this->dirty_paragraphs=array();
        $this->new_paragraphs=array();
        $this->delete_photos=array();
        $this->dirty_photos=array();
        $this->new_photos=array();
        $this->videos=array();
    }
    
    public function startNew($article_title, $category, $featured, $active, $description){
        $article_title=Helper::stripArticleTitle($article_title);
        $url_identifier=Helper::stripUrlIdentifier($article_title);
        $this->article=new Article(0, $article_title, 0, $url_identifier);
        $this->article->setCategoryId($category);
        $this->article->setFeatured($featured);
        $this->article->setActive($active);
        $this->article->setDescription($description);
        $this->init();
        $this->article_state=$this->new_article;
    }
    
    public function startUpdate($article_id){
        $this->article=$this->adminFacade->getFullArticleById($article_id);
        $this->init();
        $this->article_state=$this->unchanged_article;
    }
    
    /**
     * Loads paragraph from article, updates text, 
     * adds to dirty_paragraphs list in order 
     * to be updaed in the DataBase
     * @param float $paragraph_id
     * @param string $text 
     */
    public function editParagraph($paragraph_id ,$text){
        if ($text!=''){
            $paragraphs=$this->article->getParagraphs();
            $paragraph=$paragraphs[$paragraph_id];
            $paragraph->setText($text);
            array_push($this->dirty_paragraphs, $paragraph);
        }
    }
    
    public function addParagraph($text){
        //add constraint on database,
        //handle error throwing when inserting inconsistent data!
        if ($text!=''){
            $paragraph=new Paragraph(0, $text, 0);
            array_push($this->new_paragraphs, $paragraph);
        }
    }
    
    public function deleteParagraph($paragraph_id){
        $paragraphs=$this->article->getParagraphs();
        $paragraph=$paragraphs[$paragraph_id];
        array_push($this->delete_paragraphs, $paragraph);
    }
    
    //split in 2 methods!
    public function updateArticle($title, $category){
        $this->article->setCategoryId($category);
        $this->article->setTitle($title);
        $this->article_state=$this->dirty_article;
    }
    
    public function updateArticleFeatured($featured){
        $this->article->setFeatured($featured);
        $this->article_state=$this->dirty_article;
    }
    
    public function updateArticleActive($active){
        $this->article->setActive($active);
        $this->article_state=$this->dirty_article;
    }
    
    public function updateArticleDescription($description){
        $this->article->setDescription($description);
        $this->article_state=$this->dirty_article;
    }
    
    public function addPhoto($path, $comments, $thumbnail){
        $photo=new Pic(0, $path, $comments, 0);
        $photo->setThumbnail($thumbnail);
        array_push($this->new_photos, $photo);
    }
    
    public function editPhoto($picture_id, $comments, $thumbnail){
        $photos=$this->article->getPics();
        $photo=$photos[$picture_id];
        $photo->setComments($comments);
        $photo->setThumbnail($thumbnail);
        array_push($this->dirty_photos, $photo);
    }
    
    public function deletePhoto($picture_id){
        $photos=$this->article->getPics();
        array_push($this->delete_photos, $photos[$picture_id]);
    }
    
    public function updateVideo($video_id, $link, $comments){
        if ($video_id!=0){ 
            $video=$this->article->getVideo();
            if ($link=='') $comments='';
            $video->setLink($link);
            $video->setComments($comments);
        }
         else {
             $video=new Video(0, $link, $comments);
        }
        array_push($this->videos, $video);
    }
    
    public function getArticle() {
        return $this->article;
    }
    
    public function save(){
        $facade=$this->adminFacade;
        $facade->connect();
        $facade->autocommitFalse();
        $validation=TRUE;
        try{
            if ($this->article_state==$this->new_article){
                $result=$facade->addArticle($this->article);
                $validation=$result['result']&&$validation;
                $this->article->setId($result['id']);
            }
            
            if ($this->article_state==$this->dirty_article){
                $validation=$facade->updateArticle($this->article)&&$validation;
            }
            
            foreach($this->new_paragraphs as $paragraph){
                $validation=$facade->addParagraph($this->article->getId(), $paragraph)&&$validation;
            }
            
            foreach($this->dirty_paragraphs as $paragraph){
                $validation+=$facade->updateParagraph($paragraph)&&$validation;
            }
            
            foreach ($this->delete_paragraphs as $paragraph) {
                $validation+=$facade->deleteParagraph($paragraph)&&$validation;
            }
            
            foreach($this->new_photos as $pic){
                $validation+=$facade->addPicture($this->article->getId(), $pic)&&$validation;
            }
            
            foreach($this->dirty_photos as $pic){
                $validation+=$facade->resetThumbnails($this->article->getId())&&$validation;
                $validation+=$facade->updatePicture($pic)&&$validation;
            }
            
            foreach($this->delete_photos as $pic){
                $validation+=$facade->deletePicture($pic)&&$validation;
            }
            
            foreach($this->videos as $vid){
                $validation+=$facade->updateVideo($this->article->getId(), $vid)&&$validation;
            }
        }
        catch (Exception $e){
            $facade->rollback();
            $facade->disconnect();
            echo $e->getMessage();
            echo $e->getTrace();
        }
        if ($validation==false){
            $facade->rollback();
            $facade->disconnect();
        }
        else{
            $facade->commit();
            $facade->disconnect();
        }
    }
   
}
?>
