<?php
class Helper{
//    private static $server_name='http://localhost:8888/shifted_apps';

    public static function compareDates($date){
    if (substr($date, 0, 4)<date("Y")) return "last Year";
    else if (substr($date, 5, 2)<date("m")) return "last month";
         else if (($then=substr($date, 8, 2)) < ($now=date ("j"))) {
             $diff=$now-$then;
//             echo 'then:'.$then.' now:'.$now.'<br />';
             return $diff>1 ? $diff." days ago" : " one day ago";
             }
             else if (($then=substr($date, 11, 2))<($now=date("G"))){
                 $diff=$now-$then;
//                 echo 'then:'.$then.' now:'.$now.'<br />';
                 return $diff>1 ? $diff." hours ago" : " one hour ago";
                }
                else if (($then=  substr($date, 14, 2))<($now=date("i"))){
                    $diff=$now-$then;
//                    echo 'then:'.$then.' now:'.$now.'<br />';
                    return $diff>1 ? $diff." minutes ago" : " one minute ago";
                    }
                    else if (($then=substr($date, 17, 2))<($now=date("s"))){
                        $diff=$now-$then;
//                        echo 'then:'.$then.' now:'.$now.'<br />';
                        return $diff>1 ? $diff." seconds ago" : " one second ago";
                    }
    }
    
    public static function generateLinkFromParagraphDTO($update){
        return self::getServerName().'/article/'.$update->getArticleUrlIdentifier();
    }
    
    public static function generateLinkFromArticleDTO($articleDTO){
        return self::getServerName().'/article/'.$articleDTO->getUrlIdentifier();
    }
    
    /**
     * Used in admin part, probably list.php
     * @param type $articleDTO
     * @return type string 
     */
    public static function generateUpdateLinkFromArticleDTO($articleDTO){
        return '?cmd=update_article&article_id='.$articleDTO->getId();
    }
    
    public static function genereateCategoryLink($category){
        if ($category->getUrl_identifier()=='about') return self::getServerName().'/about';
        return self::getServerName().'/category/'.$category->getUrl_identifier();
    }
    
    public static function unserializeFromPost($object){
        return unserialize(stripslashes(htmlspecialchars_decode($object)));
    }
    
    public static function saveToSession($key, $object){
        $_SESSION[$key]=serialize($object);

    }
    
    public static function getFromSession($key) {
        return unserialize($_SESSION[$key]);
    }
    
    public static function getServerName(){
//        if ($_SERVER['SERVER_NAME']=='localhost')
//            return 'http://localhost:8888/shifted_apps';
        return "http://shiftedapps.com";
    }
    
    public static function removeStupidCss($text){
        return str_replace("background-color: rgb(255, 255, 255);", "", $text);
    }
    
    private static $id;
    
    public static function getCategoryId(){
        return self::$id;
    }
        
    public static function setCategoryId($id){
        self::$id=$id;
    }
    
    public static function stripUrlIdentifier($articleTitle){
        $url_identifier=preg_replace("/[^a-z ]+/i", "", $articleTitle);
        $url_identifier=str_replace(" ", "-", trim($url_identifier));
        $url_identifier=strtolower($url_identifier);
        return $url_identifier;
    }
    
    public static function stripArticleTitle($articleTitle){
        $articleTitle=trim(preg_replace("/[^a-z :,;!?]+/i", "", trim($articleTitle)));
        return $articleTitle;
    }
}
?>
