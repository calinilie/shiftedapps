<?php
$path=dirname(dirname(__FILE__));
require_once $path.'/model/Video.php';
require_once $path.'/model/Paragraph.php';
require_once $path.'/model/Pic.php';

class Article{
    private $id;
    private $title;
    private $paragraphs;
    private $pics;
    private $newline=PHP_EOL;
    private $added;
    private $thumbnail;
    private $video;
    private $urlIdentifier;
    private $featured;
    private $category;
    private $active;
    private $description;
    private $categoryId;
    
    public function __construct($article_id, $article_title, $added, $url_identifier){
        $this->id = $article_id;
        $this->title = $article_title;
        $this->paragraphs = array();
        $this->pics=array();
        $this->added=$added;
        $this->urlIdentifier=$url_identifier;
    }
    
    public function display(){
        $result='ArticleID: '.$this->id.$this->newline.
               'Added: '.$this->added.$this->newline.
               'Title: '.$this->title.$this->newline.
                'Featured: '.$this->featured.$this->newline;
        $result.="PARAGRAPHS:".$this->newline;
        foreach($this->paragraphs as $p){
            $result=$result.$p->display();
        }
        $result.="PICS".$this->newline;
        foreach ($this->pics as $pic)
            $result.=$pic->display();
        if (isset ($this->thumbnail)){
            $result.="THUMBNAIL:".  $this->newline;
            $result.=$this->thumbnail->display();
        }
        if (isset ($this->video)){
            $result.="VIDEO:".$this->newline;
            $result.=$this->video->display();
        }
        $result.=$this->newline;
        return $result;
    }
    
    public function getActive() {
        return $this->active;
    }

    public function setActive($active) {
        $this->active = $active;
    }
    
    public function addParagraph($paragraph){
        array_push($this->paragraphs, $paragraph);
    }
    
    public function getTitle() {
        return $this->title;
    }

    public function getParagraphs() {
        return $this->paragraphs;
    }

    public function setTitle($title) {
        $this->title = $title;
    }
    
    public function getId() {
        return $this->id;
    }

    public function getAdded() {
        return $this->added;
    }
    public function setParagraphs($paragraphs) {
        $this->paragraphs = $paragraphs;
    } 
    
    public function addPic($pic){
        array_push($this->pics, $pic);
    }
    
    public function getPics() {
        return $this->pics;
    }

    public function setPics($pics) {
        $this->pics = $pics;
    }
    public function getThumbnail() {
        return $this->thumbnail;
    }

    public function setThumbnail($thumbnail) {
        $this->thumbnail = $thumbnail;
    }
    
    public function hasThumbnail(){
        return (isset ($this->thumbnail));
    }
    
    public function getVideo() {
        return $this->video;
    }

    public function setVideo($video) {
        $this->video = $video;
    }
    
    public function hasVideo(){
        //bad workaround -> fix video persistance!
        if (isset ($this->video)){
            if ($this->video->getLink()!='') return true;
            return FALSE;
        }
        return false;
    }
    
    public function hasPics(){
        return (count($this->pics)>0);
    }
    
    public function getUrlIdentifier() {
        return $this->urlIdentifier;
    }
    
    public function getFeatured() {
        return $this->featured;
    }

    public function setFeatured($featured) {
        $this->featured = $featured;
    }
    
    public function getCategory() {
        return $this->category;
    }

    public function setCategory($category) {
        $this->categoryId=$category->getId();
        $this->category = $category;
    }
    
    public function setId($id) {
        $this->id = $id;
    }
    
    public function getCategoryName(){
        return $this->category->getName();
    }
    
    public function getDescription() {
        return $this->description;
    }

    public function setDescription($description) {
        $this->description = $description;
    }
    
    public function getCategoryId() {
        return $this->categoryId;
    }

    public function setCategoryId($categoryId) {
        $this->categoryId = $categoryId;
    }
}
?>
