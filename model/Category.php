<?php
class Category{
    private $id;
    private $url_identifier;
    private $name;
    private $description;
    private $pageTitle;
    
    public function __construct($id, $url_identifier, $name, $description, $pageTitle) {
        $this->id = $id;
        $this->url_identifier = $url_identifier;
        $this->name = $name;
        $this->description = $description;
        $this->pageTitle = $pageTitle;
    }
    
    public function getId() {
        return $this->id;
    }

    public function getUrl_identifier() {
        return $this->url_identifier;
    }

    public function getName() {
        return $this->name;
    }

    public function getDescription() {
        return $this->description;
    }

    public function getPageTitle() {
        return $this->pageTitle;
    }

    public function setName($name) {
        $this->name = $name;
    }

    public function setDescription($description) {
        $this->description = $description;
    }

    public function setPageTitle($pageTitle) {
        $this->pageTitle = $pageTitle;
    }
    
}
?>
