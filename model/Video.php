<?php
class Video {
    private $id;
    private $link;
    private $comments;
    
    function __construct($id, $link, $comments) {
        $this->id = $id;
        $this->link = $link;
        $this->comments = $comments;
    }
    
    public function getId() {
        return $this->id;
    }

    public function getLink() {
        return $this->link;
    }

    public function getComments() {
        return $this->comments;
    }

    public function getAdded() {
        return $this->added;
    }

    public function setLink($link) {
        $this->link = $link;
    }

    public function setComments($comments) {
        $this->comments = $comments;
    }
    
    public function display(){
        return $this->id.$this->link.'<br />';
    }
}
?>
