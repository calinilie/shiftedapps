<?php
////* * * * start of backtrace debug code * * *
//  $dbt = debug_backtrace();
//  echo "<div><br>= = = = = = = = Backtrace = = = = = = = =<br>\n";
//  for ( $d_b_t = 0 ; $d_b_t < count($dbt) ; $d_b_t++ ) {
//    if ( $d_b_t == 0 ) 
//      echo basename( __FILE__ ) . ' is referenced in ';
//    else {
//      echo $dbt[$d_b_t - 1]['file'] . ' is referenced in ';
//    }
//    if ( isset( $dbt[$d_b_t]['file'] ) ) {
//      echo $dbt[$d_b_t]['file'] . ' on line ';
//    }
//    if ( isset( $dbt[$d_b_t]['line'] ) ) {
//      echo $dbt[$d_b_t]['line'] . ' in a "';
//    }
//    if ( isset( $dbt[$d_b_t]['function'] ) ) {
//      echo $dbt[$d_b_t]['function'] . '"<br>' . "\n";
//    }
//  }
//  echo "<br>= = = = = = = = = = = = = = = = = = = = =<br>\n</div>";
////* * * * end of backtrace debug code * * *
class Paragraph{
    private $id;
    private $text;
    private $added;
    
    public function __construct($id, $p_text, $p_added){
        $this->id=$id;
        $this->text=$p_text;
        $this->added=$p_added;
    }
    
    public function getText() {
        return $this->text;
    }

    public function getAdded() {
        return $this->added;
    }
    
    public function display(){
        return $this->added.'<br />'.$this->text.'<br />';
    }
    
    public function getId() {
        return $this->id;
    }
    
    public function setText($text) {
        $this->text = $text;
    }
}
?>
