<?php
class Pic{
    private $id;
    private $path;
    private $comments;
    private $added;
    private $thumbnail;
    
    public function __construct($id, $path, $comments, $added) {
        $this->id = $id;
        $this->path = $path;
        $this->comments = $comments;
        $this->added = $added;
    }
    
    public function getThumbnail() {
        return $this->thumbnail;
    }
    
    public function getId() {
        return $this->id;
    }

    public function getPath() {
        return $this->path;
    }

    public function getComments() {
        return $this->comments;
    }

    public function getAdded() {
        return $this->added;
    }

    public function setPath($path) {
        $this->path = $path;
    }

    public function setComments($comments) {
        $this->comments = $comments;
    }
    
    public function display(){
        return 'Pic: '.$this->id.' path: '.$this->path.' comments: '.$this->comments.PHP_EOL;
    }
    
    public function setThumbnail($thumbnail) {
        $this->thumbnail = $thumbnail;
    }
    
    public function isThumbnail(){
        return ($this->thumbnail==1);
    }

}
?>
