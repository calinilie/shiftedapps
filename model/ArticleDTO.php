<?php
class ArticleDTO{
    private $id;
    private $title;
    private $firstParagraph;
    private $thumbnail;
    private $added;
    private $urlIdentifier;
    
    public function __construct($id, $title, $firstParagraph, $thumbnail, $added, $url_identifier) {
        $this->id = $id;
        $this->title = $title;
        $this->firstParagraph = $firstParagraph;
        $this->thumbnail = $thumbnail;
        $this->added = $added;
        $this->urlIdentifier=$url_identifier;
    }
    
    public function getId() {
        return $this->id;
    }

    public function getTitle() {
        return $this->title;
    }

    public function getFirstParagraph() {
        return $this->firstParagraph;
    }

    public function getThumbnail() {
        return $this->thumbnail;
    }
    
    public function hasThumbnail(){
        return isset ($this->thumbnail);
    }
    
    public function getAdded() {
        return $this->added;
    }
    
    public function getUrlIdentifier(){
        return $this->urlIdentifier;
    }
    
    public function display(){
        $newline="<br />";
        
        $result=$this->added." ".$this->title.$newline.
                $this->firstParagraph->display();
        if (isset($this->thumbnail)) $result.=$this->thumbnail->display();
        return $result;
    }
    
    public function getPreview(){
        return substr(strip_tags($this->firstParagraph->getText()), 0, 300).'...';
    }

}
?>
