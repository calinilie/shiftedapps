<?php
class ParagraphDTO{
    private $articleTitle;
    private $articleUrlIdentifier;
    private $added;
    private $text;
    
    
    function __construct($articleTitle, $article_id, $added, $text) {
        $this->articleTitle = $articleTitle;
        $this->articleUrlIdentifier = $article_id;
        $this->added = $added;
        $this->text=$text;
    }

    
    public function getArticleTitle() {
        return $this->articleTitle;
    }

    public function getAdded() {
        return $this->added;
    }
    
    public function getArticleUrlIdentifier() {
        return $this->articleUrlIdentifier;
    }
    
    public function getText() {
        return substr(strip_tags($this->text), 0, 90).'...';
    }

}
?>
