<?php
require_once '../datasource/FacadeUser.php';
require_once '../Logic/Helper.php';


if (isset ($_POST['limit'])) $limit=$_POST['limit'];
else $limit=5;

$updates=FacadeUser::getInstance()->getUpdates($limit);
//echo date("Y-m-d G:i");
?>


    <div class="latest_posts">
    	<span class="shiftedapps_icon"></span>
    	<a href="#" class="shiftedapps_posts">ShiftedApps | Updates</a>
        <?php foreach ($updates as $update):?>
        <div class="shiftedapps_article">
            <a href="<?php echo Helper::generateLinkFromParagraphDTO($update) ?>"></a>
                <div class="shiftedapps_article_title"><p><?php echo $update->getArticleTitle(); ?></p></div>
                <p>
                    <?php echo $update->getText(); ?>
                </p>
        	<span class="latest_readmore">
            	<a href="<?php echo Helper::generateLinkFromParagraphDTO($update) ?>">
                	Read more
                </a>
            	<span class="readmore_icon"></span>
            </span>
       
                <div class="time_since_update"><p>updated <?php /*substr($update->getAdded(), 0, 16);*/ echo "  ".  Helper::compareDates($update->getAdded()); ?></p></div>
            <!-- end .shiftedapps_article --> </div>
    <?php endforeach;?>
   <!-- end .latest_posts --> </div> 

