<?php
$path=dirname(dirname(__FILE__));
require_once $path.'/Logic/Helper.php';

$article_title=$_POST['title'];
$article_category=$_POST['category'];
$category_url=$_POST['category_url'];
$dateAdded=$_POST['date_added'];
?>
<article class="big_article">
    <header class="big_title"> 
        <div class="link_to_articles">
            <a href="<?php echo Helper::getServerName() ?>">Articles</a>
        </div>
        <div class="link_to_category">
                <a href="<?php echo $category_url ?>"><?php echo $article_category ?></a>
        </div>
        <div class="clearfloat">
            </div>
        <div class="big_title_size">
            <p><?php echo $article_title ?></p>
        </div>
        <div>
            <div class="big_postMeta">
            <time datetime="<?php echo $dateAdded ?>" class="post-date" pubdate>
                    Posted: <span class="posted"><?php echo $dateAdded ?></span>
            </time>
            <div class="big_social_media"> 
            </div>
            <div class="clearfloat">
        </div>
        </div>
    </header>
