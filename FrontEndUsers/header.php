<?php
$path=dirname(dirname(__FILE__));
require_once $path.'/Logic/Helper.php';
$category=$_POST['category_id'];
?>
<div class="header">
    
    <div class="logo">
            <a href="<?php echo Helper::getServerName() ?>/index.php">
        <span></span></a>
        <!-- end .logo --></div>
        <div class="search-box">
        	<form action="/search" method="get" class="searchForm">
                <input type="text" class="textfield_search" id="search_main" name="search" value="" placeholder="Search articles &amp; apps">
                <button type="submit" class="submit"></button>
            </form>
        <!-- end .search-bar --></div>
        <div class="clearfloat"></div>
        <div class="navigation">
  	<ul class="nav">
            <li><a href="<?php echo Helper::getServerName() ?>/index.php" class="<?php echo $category==0 ? 'current': '' ?>">Home</a></li>
            <li><a href="<?php echo Helper::getServerName() ?>/category/android"  class="<?php echo $category==1 ? 'current': '' ?>">Android</a></li>
            <li><a href="<?php echo Helper::getServerName() ?>/category/ios"  class="<?php echo $category==2 ? 'current': '' ?>">iOS</a></li>
            <li><a href="<?php echo Helper::getServerName() ?>/about"  class="<?php echo $category==3 ? 'current': '' ?>">About</a></li>
        </ul>
    <!--end of .navigation--></div>
</div>