<?php
require_once '../datasource/FacadeUser.php';
$article=1;
if (isset ($_POST['article'])) $article=$_POST['article'];
$paragraphs = FacadeUser::getInstance()->getParagraphsForArticle($article);?>
<ul>
    <?php foreach($paragraphs as $p) :?>
    <li> <?php echo $p->getText(); ?>  </li>
    <?php endforeach;?>
</ul>
