<?php
$path=dirname(dirname(__FILE__));
require_once $path.'/Logic/Helper.php';
require_once $path.'/model/ArticleDTO.php';
require_once $path.'/model/Pic.php';
require_once $path.'/model/Paragraph.php';
$articles=Helper::unserializeFromPost($_POST['articles']);
$category=$_POST['category'];
$lowerLimit=$_POST['lowerLimit'];
$server_name=Helper::getServerName();
?>
<?php foreach($articles as $articleDTO): ?>
<?php $article_url=Helper::generateLinkFromArticleDTO($articleDTO); ?>
<article class="post">
    <header>
            <h1 class="postTitle">
                    <a href="<?php echo $article_url ?>" ><?php echo $articleDTO->getTitle(); ?></a>
            </h1>
        <div class="postMeta">
            <time datetime="<?php echo $articleDTO->getAdded(); ?>" class="post-date" pubdate>
                    Posted: <span class="posted"><?php echo $articleDTO->getAdded(); ?></span>
            </time>
            <span class="postCategory"> 

            </span>
        </div>
    </header>
    <?php if ($articleDTO->hasThumbnail()):?>
    <figure class="postImage">
            <a href="<?php echo $article_url ?>">
            <img class="imgPosted" src="<?php echo $server_name.$articleDTO->getThumbnail()->getPath(); ?>" title="<?php echo $articleDTO->getTitle();?>" alt="<?php echo $articleDTO->getTitle();?>" />
            </a>
    </figure>
    <?php endif; ?>
    <div class="postContent">
            <p>
                <?php echo $articleDTO->getPreview(); ?>
        </p>
    </div>

    <footer class="postInfo">
            <span class="foLeft">
                <a href="<?php echo $article_url ?>">Read more</a>
            <span class="readmore_icon"></span>
        </span>

<!--        <div class="foRight">
            <span class="comments_icon">999</span>
            <a href="#">Comments</a>

        </div>-->
            </footer>
    <span class="clearfloat"></span>
</article>
<?php endforeach; ?>
<button id="load_more">More stories</button>
<input type="hidden" id="category" value="<?php echo $category ?>" />
<input type="hidden" id="lowerLimit" value="<?php echo $lowerLimit?>" />
<script type="text/javascript">
function list_scripts(){ 
    <?php if ($category!=3): ?>
        var top = $('#social_media').offset().top - parseFloat($('#social_media').css('marginTop').replace(/auto/, 0));
        $(window).scroll(function (event) {
        // what the y position of the scroll is
        var y = $(this).scrollTop();

        // whether that's below the form
        if (y >= top) {
          // if so, ad the fixed class
          $('#social_media').addClass('fixed');
        } else {
          // otherwise remove it
          $('#social_media').removeClass('fixed');
        }
        });

        var topT = $('#twitter_feed').offset().top;
        var topTotal = (topT - 74);
        $(document).scroll(function (event) {
        // what the y position of the scroll is
        var y = $(this).scrollTop();

        // whether that's below the form
        if (y >= topTotal) {
          // if so, ad the fixed class
          $('#twitter_feed').addClass('fixed');
        } else {
          // otherwise remove it
          $('#twitter_feed').removeClass('fixed');
        }
        });
    <?php endif; ?>
    $('#load_more').click(function(event){
       event.preventDefault();
       var category=$("#category").val();
       var lowerLimit=parseInt($("#lowerLimit").val());
       $.ajax({
           url:"<?php echo Helper::getServerName()?>/FrontEndUsers/loadmore.php",
           data: {category: category, lower: lowerLimit, limit: 7},
           success: function(response){
               appendArticles(response);
               updateLowerLimit(lowerLimit+7);
           }
       });
    });
   
    function appendArticles(response){
        var stuff=$.parseJSON(response);
        if (stuff.length > 0)
        {
            $.each(stuff, function(i, val){
                var article=$('<article class="post"></article>').hide();
                    //header: title + date
                     var header=$("<header><header>").appendTo(article);
                        var postTitle=$('<h1 class="postTitle"></h1>').appendTo(header);
                            $('<a href="'+val.link+'" >'+val.title+'</a>').appendTo(postTitle);
                        var postMeta=$('<p class="postMeta"></p>').appendTo(header);
                            $('<time datetime="'+val.added+'" class="post-date" pubdate>Posted: <span class="posted">'+val.added+'</span></time>').appendTo(postMeta);
                    //thumbnail if exists
                     if (val.thumbnail!=null){
                     var figure=$(' <figure class="postImage"></figure').appendTo(article);
                        var thumbnail_link=$('<a href="'+val.link+'"></a>').appendTo(figure);
                            $('<img class="imgPosted" src="'+val.thumbnail+'" title="'+val.title+'" alt="'+val.title+'" />').appendTo(thumbnail_link);
                     }
                    //first paragraph
                     var postContent=$('<div class="postContent"><p>'+val.paragraph+'</p></div>').appendTo(article);
                    //read more, add comments
                     var postInfo=$('<footer class="postInfo"></footer>').appendTo(article);
                        var foLeft=$('<span class="foLeft"></span>').appendTo(postInfo);
                            $('<a href="'+val.link+'">Read more</a><span class="readmore_icon"></span>').appendTo(foLeft);
                article.insertBefore($('#load_more'));
                article.fadeIn(400);
                console.log(val.title+" "+val.added);
             });
        }
        else 
        {
            $('<div><p>Wohooo! You have read all our articles in this category, stay tuned!</p></div>').insertBefore($('#load_more'));
            $('#load_more').attr("disabled", true);
        }
    }
    
    function updateLowerLimit(value){
        $("#lowerLimit").val(value);
    }

}
</script>
<script type="text/javascript" src="<?php echo Helper::getServerName() ?>/js/plugin_files/jquery-1.7.1.min.js"></script>
<script type="text/javascript">
    function addLoadEvent(func) {
      var oldonload = window.onload;
      if (typeof window.onload != 'function') {
        window.onload = func;
      } else {
        window.onload = function() {
          if (oldonload) {
            oldonload();
          }
          func();
        }
      }
    }
    try{
        addLoadEvent(featured_scripts);
    }
    catch(err){
        console.log(err);
    }
    addLoadEvent(list_scripts);
</script>