<?php
$path=dirname(dirname(__FILE__));
require_once $path.'/Logic/Helper.php';


$category=$_POST['category'];

?>

<div class="headline">
<div class="accordion"></div>
<div id="mobileSlider">
<div id='slider' class='swipe'>
  <ul>
    <li style='display:block'><div>1</div></li>
    <li style='display:none'><div>2</div></li>
    <li style='display:none'><div>3</div></li>
    <li style='display:none'><div>4</div></li>
    <li style='display:none'><div>5</div></li>
    <li style='display:none'><div>6</div></li>
  </ul>
</div>
<a href='#' onclick='slide.prev();return false;'>prev</a> 
<a href='#' onclick='slide.next();return false;'>next</a>
</div>
</div>
<script type="text/javascript">
   function featured_scripts(){
        $.getScript("<?php echo Helper::getServerName() ?>/js/plugin_files/jquery.gridAccordion.min.js", function(){
           $('.accordion').gridAccordion({
            xmlSource:'<?php echo Helper::getServerName() ?>/FrontEndUsers/featured_source.php?category=<?php echo $category?>', 
            width:960, height:320, 
            distance:1, 
            closedPanelWidth:100, 
            closedPanelHeight:50, 
            slideshow:true, 
            alignType:'centerCenter', 
            columns:3, openPanelDelay:10, 
            openPanelOnClick:false, 
            openPanelOnMouseOver:true, 
            slideshowDelay:4000, 
            openedPanelWidth:600, 
            openedPanelHeight:250, 
            preloadPanels:true, 
            captionWidth:600, 
            captionHeight:80, 
            captionLeft:0, 
            captionTop:180}); 
        }); 
    }
</script>
