<?php
$path=dirname(dirname(__FILE__));
require_once $path.'/Logic/Helper.php';
require_once $path.'/model/Article.php';
$article=Helper::unserializeFromPost($_POST['article']);
$pics=$article->getPics();
$picsSize=count($pics);
?>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=400534249971729";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));
</script>
<div class="post">
    <?php if ($article->hasThumbnail()): ?>
    <figure class="postImage">
        <img class="imgPosted" src="<?php echo Helper::getServerName().$article->getThumbnail()->getPath() ?>" title="<? echo $article->getThumbnail()->getComments()?>" alt="<? echo $article->getThumbnail()->getComments()?>"/>
    </figure>
    <?php endif;?>
    <?php if ($article->hasPics()): ?>
    <div class="<?php echo ($picsSize>0) ? "image_carousel" : "slider-wrapper" ?>">
        <div id="carousel">
            <?php foreach ($pics as $picture) :?>
                <img class="imageBtn" src="<?php echo Helper::getServerName().$picture->getPath(); ?>" title="<?php echo $picture->getComments();?>" alt="<?php echo $picture->getComments(); ?>"/>
            <?php endforeach;?>
        </div>
        <?php if ($picsSize>0) : ?>
        <div class="clearfix"></div>
        <a class="prev" id="foo2_prev" href="#"><span>prev</span></a>
        <a class="next" id="foo2_next" href="#"><span>next</span></a>
        <div class="pagination" id="foo2_pag"></div>
        <?php endif; ?>
    </div>
    <?php endif; ?>
    <?php foreach ($article->getParagraphs() as $paragraph): ?>
        <div class="added">Added <?php echo Helper::compareDates($paragraph->getAdded()) ?></div>
        <div class="postContent">   
            <p><?php echo $paragraph->getText() ?></p>
        </div>
    <?php endforeach;?>
    <?php if ($article->hasVideo()) :?>
        <?php $video=$article->getVideo(); ?>
        <div class="video">
            <iframe class="youtube-player" type="text/html" src="http://www.youtube.com/embed/<?php echo substr($video->getLink(), -11) ?>?rel=0&autohide=1&theme=light&color=white" frameborder="0"></iframe>
            <div class="video_comment"><?php echo $video->getComments(); ?></div>
        </div>
    <?php endif;?>
</div>
</article>
<div class="fb-comments" data-href="<?php echo Helper::getServerName().'/article/'.$article->getUrlIdentifier(); ?>" data-num-posts="2" data-width="640px"></div>
<script type="text/javascript" src="<?php echo Helper::getServerName() ?>/js/plugin_files/jquery-1.7.1.min.js"></script>
<?php if (count($pics)>0): ?>
<script type="text/javascript" src="<?php echo Helper::getServerName() ?>/js/jquery.lightbox-0.5.pack.js"></script>
<script type="text/javascript" src="<?php echo Helper::getServerName() ?>/js/jquery.carouFredSel-5.5.0-packed.js"></script>
<script type="text/javascript">
$(document).ready(function() {

    $("#carousel").carouFredSel({
        height:   100,
        width: 600,
        infinite :false,
        circular:false,
        auto 	: false,
        prev	: {	
                button	: "#foo2_prev",
                key		: "left"
        },
        next	: { 
                button	: "#foo2_next",
                key		: "right"
        },
        pagination	: "#foo2_pag"
    });

    $('#carousel img').lightBox({
        keyToClose: 'escapeKey',
        imageLoading: '<?php echo Helper::getServerName() ?>/css/graphics/lightbox/lightbox-ico-loading.gif',
        imageBtnClose: '<?php echo Helper::getServerName() ?>/css/graphics/lightbox/lightbox-btn-close.gif',
        imageBtnPrev: '<?php echo Helper::getServerName() ?>/css/graphics/lightbox/lightbox-btn-prev.gif',
        imageBtnNext: '<?php echo Helper::getServerName() ?>/css/graphics/lightbox/lightbox-btn-next.gif',
        imageBlank:	'<?php echo Helper::getServerName() ?>/css/graphics/lightbox/lightbox-blank.gif'
    });


    $(document).bind('keyup', function(e) {
        if(e.keyCode == 27) {
            $('#lightbox-secNav-btnClose').click();
            $.lightBox().close();
        }
    });  
});
</script>
<?php endif; ?>