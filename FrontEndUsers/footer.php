<?php
$path=dirname(dirname(__FILE__));
require_once $path.'/Logic/Helper.php';
?>

      	<div class="footer_wrapper">
  		<div class="shadow_bar">
   	 	</div>
    	<div class="footer_content">
        	<div class="footer_left">
            	<div class="footer_logo">
                	<a href="#"></a>
                </div>
            </div>
            
            <div class="footer_middle">
            	<p class="footer_text">
                	Menu
                </p>
                <nav class="nav_footer">
                    <a href="<?php echo Helper::getServerName() ?>/index.php">Front Page</a><br />
                    <a href="<?php echo Helper::getServerName() ?>/category/android">Android apps review</a><br/>
                    <a href="<?php echo Helper::getServerName() ?>/category/iOS">iOS apps review</a><br />
                    <a href="#">Posts</a><br />
                    <a href="<?php echo Helper::getServerName() ?>/about">About</a><br />
                    <a href="mailto:mail@shiftedapps.com?subject=We would like to hire you">Work with us</a><br />
                    <a href="mailto:mail@shiftedapps.com">Contact</a><br />
                    <a href="mailto:mail@shiftedapps.com?subject=Suggestions">Suggestions</a>
                </nav>
                
            </div>
            
            <div class="footer_right">
                <div id="mail_stamp">
                	<a href="mailto:mail@shiftedapps.com"></a>
                </div>
                <div id="twitter_stamp">
                	<a href="https://twitter.com/ShiftedApps"></a>
                </div>
                <div id="facebook_stamp">
                	<a href="https://www.facebook.com/ShiftedApps"></a>
                </div>
                <div id="googleplus_stamp">
                	<a href="https://plus.google.com/104728936089671382092/posts"></a>
                </div>
            </div>
            
            <div class="clearfloat">
            </div>

            <!--<nav class="nav_footer">© 2012 Shiftedapps.com  All rights reserved.     Main |  Android app reviews  |  iOS app reviews  |  About  |  Contact  |  Suggestions
			</nav>-->
        
    	<!-- end .footer_content --></div>
    <!-- end .footer --></div>
