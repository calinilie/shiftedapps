<?php
if (!isset ($_POST['page_title']) || ($_POST['page_title']=='')) $title='Shifted Apps - A world of apps';
else {
    $title=$_POST['page_title'];
    if (strpos($title, "Shifted Apps")===false) $title.=' | Shifted Apps';
}

if (!isset ($_POST['description'])) $description="Mainly mobile apps reviews on both android and ios platforms, giving our own opinion as app consumers. Tech blog. ";
else $description=$_POST['description'];
?>
<title><?php echo $title ?></title>
<meta name="description" content="<?php echo $description ?>" />