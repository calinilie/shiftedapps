<?php
$path=dirname(dirname(__FILE__));
require_once $path.'/datasource/FacadeUser.php';
require_once $path.'/Logic/Helper.php';

$category=$_GET['category'];
if (isset ($_GET['limit'])) $limit=$_GET['limit'];
else $limit=6;
$featured=1;
$articles=FacadeUser::getInstance()->getArticlesDTO($category, $limit, $featured);

header("Content-type: text/xml"); 
echo '<?xml version="1.0" encoding="ISO-8859-1"?>';
?>
<accordion>
<?php foreach ($articles as $article): ?>
    <?php if ($article->hasThumbnail()) :?>
    <panel>
        <path><?php echo Helper::getServerName().$article->getThumbnail()->getPath(); ?></path>
        <caption><![CDATA[<h1 class="panel_title"><a href="<?php echo Helper::generateLinkFromArticleDTO($article) ?>"><?php echo $article->getTitle()?></a></h1>]]></caption>
        <link><?php echo Helper::generateLinkFromArticleDTO($article) ?></link>
    </panel>
    <?php endif; ?>
<?php endforeach; ?>
</accordion>
