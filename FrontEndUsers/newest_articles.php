<?php
require_once '../datasource/FacadeUser.php';
require_once '../Logic/Helper.php';


if (isset($_POST['limit'])) $limit=$_POST['limit'];
else $limit=5;
$category=0;
$featured=0;

$articles_dto=FacadeUser::getInstance()->getArticlesDTO($category, $limit, $featured);
?>

<div class="latest_articles">
    <h2>Newest Articles Widget</h2>
    <?php foreach($articles_dto as $article): ?>
    <div class="new_article">
        <div class="title"><?php echo $article->getTitle() ?></div>
        <div class="time_since_update">added <?php echo Helper::compareDates($article->getAdded()); ?></div>
    </div>
    <?php endforeach; ?>
    
</div>
