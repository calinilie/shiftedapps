<?php
$path=dirname(dirname(__FILE__));
require_once $path.'/Commands/Commands.php';
require_once $path.'/Logic/Helper.php';
$commands=Commands::getInstance();

if (isset($_GET['lower'])) $lowerLimit=$_GET['lower'];
else $lowerLimit=0;

if (isset($_GET['limit'])) $limit=$_GET['limit'];
else echo 'err';

if (isset($_GET['category'])) $category=$_GET['category'];
else $category=0;

$data=array('lower'=>$lowerLimit,
            'limit'=>$limit,
            'category'=>$category);
$result=$commands->executeCommand('loadMore', $data);
echo $result;
?>

