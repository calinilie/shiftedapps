<?php
$path=dirname(dirname(__FILE__));
require_once $path.'/Blocks/Block.php';
require_once $path.'/Logic/Helper.php';
require_once $path.'/model/Article.php';
require_once $path.'/model/Paragraph.php';
require_once $path.'/model/Video.php';

if (isset ($_POST['article'])){
        $article=Helper::unserializeFromPost($_POST['article']);
}  
?>
<script src="http://code.jquery.com/jquery-latest.js"></script>
<style>
    h3{
        display: block;
        margin:0px;
    }
    .clear{
        clear:both;
    }
    .left_side{
        width:16%;
        float:left;
        padding:20px 2%;
    }
    
    .left_menu{
        width:inherit;
        position:fixed;
    }
    
    .right_side{
       width: 76%;
       float: left;
       padding: 20px 2%;
       max-width: 960px;
    }
    
    li{
        list-style: none;
        width:100%;
    }
    button{
        width:100%;
        margin:5px 0px;
        font-size: 16px;
        cursor: pointer;
    }
    fieldset{
        border:none;
    }
    .radio{
        float:left;
        width:15px;
    }
    p{
        margin:0px;
        float: left;
    }
    label{
        font-family: Arial;
        font-size: 20px;
    }
    .option{
        display: none;
        margin: 5px 0px;
        border-top: 2px solid greenyellow;
        padding: 5px 0px;
    }
    .active{
        border-color: greenyellow;
    }
    a{
        display: block;
    }
    .submit{
        font-size: 20px;
        color:green;
        font-weight: bold;
    }
    .delete_paragraph{
/*        float:left;*/
        margin: 0px;
        width:200px;
        border:1px solid transparent;
        padding:0px;
    }
    .paragraph_entry{
        margin:20px 0px;
    }
    
    textarea{
        display: block;
    }
</style>
<?php if (isset ($article)) :?>
<form action="<?php echo Helper::getServerName();?>/admin.php" method="post">
    <div class="left_side">
        <div class="left_menu">
            <li><button class="active" onclick="show(event, '#general_options', this)">General</button></li>
            <li><button onclick="show(event, '#paragraphs_list', this)">Paragraphs</button></li>
            <li><button onclick="show(event, '#pictures', this)">Pictures</button></li>
            <li><button onclick="show(event, '#videos', this)">Video</button></li>
            <input class="submit" type="submit" value="Submit" onclick="return validateArticle();"/>
        </div>
    </div>
    <div class="right_side">
    <div class="option" id="general_options" style="display:block;">
        <h3>General options, ID: <?php echo $article->getId(); ?></h3>
        <fieldset>
            <div>
                <label for="article_title">Title</label>
                <input type='text' name="article_title" value="<?php echo $article->getTitle(); ?>" size="50"/>
                <div class="clear"></div>
            </div>
            <div class="categories">
                <label for="article_catgeory">Category</label>
                <div><input class="radio" type="radio" name="article_category" value="1" <?php echo ($article->getCategoryId()==1) ? 'CHECKED' : '' ?>/><p>android</p></div>
                <div class="clear"></div>
                <div><input class="radio" type="radio" name="article_category" value="2" <?php echo ($article->getCategoryId()==2) ? 'CHECKED' : '' ?>/><p>ios</p></div>
                <div class="clear"></div>
                <div><input class="radio" type="radio" name="article_category" value="3" <?php echo ($article->getCategoryId()==3) ? 'CHECKED' : '' ?>/><p>about</p></div>
                <div class="clear"></div>
            </div>
            <div>
                <label for="article_featured">Featured</label>
                <input type="checkbox" name="article_featured" <?php echo $article->getFeatured()==1 ? 'CHECKED' : '' ?>/>
                <div class="clear"></div>
            </div>
            <div>
                <label for="article_active">Active</label>
                <input type="checkbox" name="article_active" <?php echo $article->getActive()==1 ? 'CHECKED' : '' ?>/>
                <div class="clear"></div>
            </div>
            <div>
                <label for="article_description">Article page Description</label>
                <textarea name="article_description" cols="50" rows="5"><?php echo $article->getDescription() ?></textarea>
                <div class="clear"></div>
            </div>
        </fieldset>
    </div>
    <div class="option" id="paragraphs_list">
        <h3>Paragraphs</h3>
        <a id="add_paragraph" href="javascript:void(0)">+ Add Paragraph</a>
        <?php $paragraphs=$article->getParagraphs(); 
        foreach ($paragraphs as $id=>$paragraph) :?>
        <div class="paragraph_entry">
            <p>Last modified <?php echo $paragraph->getAdded() ?></p>            
            <div class="clear"></div>
            <div><button class="delete_paragraph">Delete form Database</button></div>
            <input class="paragraph_id" type="hidden" name="article_paragraphs_ids[]" value="<?php echo $id ?>" />
            <textarea class="paragraph_text" name="article_paragraphs[]" cols="111" rows="10"><?php echo $paragraph->getText() ?></textarea>
            <input class="paragraph_state" type="hidden" name="article_paragraphs_state[]" value="unchanged" />
        </div>
        <?php endforeach; ?>
        <div class="clear"></div>
    </div>
    <div class="option" id="pictures">
        <h3>Pictures</h3>
        <?php  
            $data['pics']=serialize($article->getPics());
            Block::renderBlock(Helper::getServerName()."/FrontEndAdmin/pics.php", $data); 
        ?>
        <?php Block::renderBlock(Helper::getServerName()."/FrontEndAdmin/upload.php", NULL); ?>
        <div class="clear"></div>
    </div>
    <div class="option" id="videos">
        <?php $video=$article->getVideo();
        if (isset($video)){
            $link=$video->getLink();
            $comments=$video->getComments();
            $video_id=$article->getId();
        }
        else {
            $link='';
            $comments='';
            $video_id=0;
        }
        ?>
        <h3>Videos</h3>
        <input type="hidden" name="video_state" value="unchanged"/>
        <input type="hidden" name="video_id" value="<?php echo $video_id ?>"/>
        <label for="video_link">Video Link</label>
        <input type="text" name="video_link" size="50" value="<?php echo $link ?>"/>
        <h4 style='margin:0px'>Comments (optional):</h4>
        <textarea class="video_comments" name='video_comments' cols='40' rows='5'><?php echo $comments ?></textarea>
        <div class="clear"></div>
    </div>
    <input type="hidden" name="article_state" value="unchanged" />
    <input type="hidden" name="cmd" value="do_update_article"/>
    </div>
</form>
<?php else :?>
    <h2>Some error occurred! Try again and tell Calin about it :)</h2>
<?php endif;?>
<script type="text/javascript">
    $(".paragraph_text").keydown(function(event){
       var dom = $(this).parent().children(".paragraph_state"); 
       checkState(dom);
    });
    
    $('input[name="article_title"]').keydown(function(event){
       $('input[name="article_state"]').val("changed");
    });
    
    $('input[name="article_category"]').click(function(){
       $('input[name="article_state"]').val("changed");
    });
    
    $('input[name="article_featured"]').click(function(){
       $('input[name="article_state"]').val("changed");
    });
    
    $('input[name="article_active"]').click(function(){
       $('input[name="article_state"]').val("changed");
    });
    
    $('textarea[name="article_description"]').keydown(function(event){
       $('input[name="article_state"]').val("changed");
    });
    
    $('input[name="video_link"]').keydown(function(){
       $('input[name="video_state"]').val("changed");
    });
    
    $('.video_comments').keydown(function(event){
       $('input[name="video_state"]').val("changed");
    });
    
    $('#add_paragraph').click(function(){
       var newParagraph=$('<div class="paragraph_entry" style="display:none;"></div>');
       $('<input class="paragraph_id" type="hidden" name="article_paragraphs_ids[]" value="0" />').appendTo(newParagraph);
       $('<textarea class="paragraph_text" name="article_paragraphs[]" cols="111" rows="10"></textarea>').appendTo(newParagraph);
       $('<input class="paragraph_state" type="hidden" name="article_paragraphs_state[]" value="new" />').appendTo(newParagraph);
       $('<a style="float:right;" href="javascript:void(0)" onclick="remove_this(this);">-Remove Entry</a>').appendTo(newParagraph);
       $('<div class="clear"></div>').appendTo(newParagraph);
       $(newParagraph).appendTo('#paragraphs_list');
       $(newParagraph).slideDown(200);
    });
    
    function remove_this(button){
        $(button).parent().remove();
        return false;
    }
    
    function show(event, target_id, button){
        event.preventDefault();
        $(target_id).slideToggle(200);
        $(button).toggleClass('active');
    };
    
    jQuery('.delete_paragraph').click(function(event){
        event.preventDefault();
        var dom=jQuery(this);
        registerForDelete(dom, 'article_paragraphs_state[]');
    });
</script>
