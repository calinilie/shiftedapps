<?php
//needs architecture refactoring
//create 2 interfaces (iFacadeUser and iFacadeAdmin) and create a facade that implements both of them
//use interfaces to retrieve data
//hopefully it works in PHP
$path=dirname(dirname(__FILE__));
require_once $path.'/Logic/Helper.php';
require_once $path.'/model/Pic.php';
$pictures = Helper::unserializeFromPost($_POST['pics']);
$server_name=Helper::getServerName();
?>
<style>
    .delete_picture{
        float:right;
        margin: 0px;
        width:70px;
        border:1px solid transparent;
    }
    .delete_active{
        border:1px solid red;
        background-color: #C20606;
        color:white;
    }
</style>
<div>
<?php foreach ($pictures as $pic):?>
    <div class="uploaded_picture">
        <img src='<?php echo $server_name.'/'.$pic->getPath() ?>' style='width:301px; height:169px;' />
        <div style="padding:5px">
            <label style='font-size: 15px;' for='pic_thumbnail'>Thumbnail</label>
            <input type='radio' name='pic_thumbnail' value='<?php echo $pic->getPath() ?>' <?php echo ($pic->isThumbnail()==true) ? 'CHECKED' : '' ?>/>
            <button class="delete_picture">Delete</button>
            <div class="clear"></div>
        </div>
        <input type='hidden' value='<?php echo $pic->getId()?>' name='pic_id[]' />
        <input type='hidden' value='unchanged' name='pic_state[]' />
        <input type='hidden' value='<?php echo $pic->getPath() ?>' name='pic_path[]' />
        <h4 style='margin:0px'>Comments (optional):</h4>
        <textarea class="pic_comments" name='pic_comments[]' cols='40' rows='5'><?php echo $pic->getComments() ?></textarea>
    </div>
<?php endforeach;?>
<div class="clear"></div>
</div>
<script>
    //register events and do something
    jQuery('.delete_picture').click(function(event){
        event.preventDefault();
        var dom=jQuery(this);
        registerForDelete(dom,'pic_state[]');
    });
    
    $('.pic_comments').keydown(function(event){
        var dom=jQuery(this).parent().find('input[name="pic_state[]"]');
        checkState(dom);
    });
    
    $('input[name="pic_thumbnail"]').click(function(){
        var dom=jQuery(this).parent().parent().find('input[name="pic_state[]"]');
        checkState(dom);
    });
    
    //define action for different events
    function checkState(dom){
        var state=$(dom).attr('value');
        var element = 'element';
        if ($(dom).attr('name')=='pic_state[]') element='picture';
        if ($(dom).attr('name')=='article_paragraphs_state[]') element='paragraph';
        if (state=='delete') alert('You have chosen to delete this '+element+'. Any changes will not be registered, because the item will be deleted. \n\nPress Delete button again to "undelete" it, then you will be able to edit it.');
        else $(dom).val('changed');
    }
    
    function registerForDelete(dom, inputName){
        var state=jQuery(dom).parent().parent().find('input[name="'+inputName+'"]');
        switch (jQuery(state).val()){
            case ('unchanged'):
                jQuery(state).val('delete');
                break;
            case ('changed'):
                jQuery(state).val('delete');
                break;
            case ('delete'):
                jQuery(state).val('changed');
                break;
        }
        jQuery(dom).toggleClass('delete_active');
    }
</script>