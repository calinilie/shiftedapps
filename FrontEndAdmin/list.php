<?php
$path=dirname(dirname(__FILE__));
require_once $path.'/datasource/FacadeUser.php';
require_once $path.'/Logic/Helper.php';

$articles = FacadeUser::getInstance()->getArticlesDTO(-1, 400, 0);
?>
<style>
    .container{
       margin:0 auto;
       width:960px;
    }
    
    .odd{
        background-color: #E6F8FC;
    }
    
    .even{
        background-color: #FCEEE6;
    }
    
    .odd:hover, .even:hover{
        background-color: transparent;
    }
    
    .center{
        text-align: center;
    }
    
    table{
        width:inherit;
    }
    a{
        display: block;
    }
</style>
<div class="container">
    <a href="?cmd=logout">[logout]</a>
    <a href="?cmd=insert_article">+ Add Article </a>
<table>
    <tr>
        <th>Article ID</th>
        <th>Title</th>
        <th>Added</th>
        <th>Click that link</th>
    </tr>
<?php $i=0;
foreach ($articles as $article):?>
    <tr class="<?php echo $i%2==0 ? 'even' : 'odd' ?>">
        <td class="center"><p class="article_id"><?php echo $article->getId(); ?></p></td>
        <td><p class="article_title"><?php echo $article->getTitle(); ?></p></td>
        <td><p class="added"><?php echo $article->getAdded() ?></p></td>
        <td class="center"><a href="<?php echo Helper::generateUpdateLinkFromArticleDTO($article)?>">update</a></td>
    </tr>
<?php $i++;
endforeach; ?>
</table>
</div>
