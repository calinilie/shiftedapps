<?php
$path=dirname(dirname(__FILE__));
require_once $path.'/Logic/Helper.php';
?>
<style>
        .add_file{
        border:none;
        background:none;
        font-weight: bold;
        float:left;
        background:url("css/graphics/addfile.png");
        background-repeat: no-repeat;
        background-position: top center;
        position:relative;
        cursor: pointer;
        padding: 32px 20px 0px;
    }


    .start_upload{
        border:none;
        background:none;
        font-weight: bold;
        background:url("css/graphics/upload.png");
        background-repeat: no-repeat;
        background-position: top center;
        float:left;
        position:relative;
        cursor: pointer;
        padding: 32px 30px 0px;
    }
    .uploaded_picture{
        width:305px;
        height:355px;
        margin:3px;
        overflow: hidden;
        float:left;
    }
</style>
<script type="text/javascript" src="<?php echo Helper::getServerName() ?>/js/plupload.full.js"></script>
<div id="uploader">
    <div id="filelist"></div>
    <div class="clear"></div>
    <div class="add_file_container"><a id="pickfiles" class="add_file" href="#">[Browse]</a></div>
    <div class="start_upload_container"><a id="uploadfiles" class="start_upload" href="#">[Upload]</a></div>
</div>
<script type="text/javascript">
jQuery(function() {
    var uploader = new plupload.Uploader({
            runtimes : 'html5',
            browse_button : 'pickfiles',
            container : 'uploader',
            max_file_size : '10mb',
            url : '<?php echo Helper::getServerName() ?>/Commands/upload.php',
             filters :[
             {title:'Allowed files',extensions:'jpeg,jpg,png,tif,tiff,aiff,aif,aifc,mpeg4,mp4,wav'}
         ]
    });

    uploader.bind('Init', function(up, params) {
            //jQuery('#filelist').html("<div>Current runtime: " + params.runtime + "</div>");
    });

    jQuery('#uploadfiles').click(function(e) {
            uploader.start();
            e.preventDefault();
    });

    uploader.init();

    uploader.bind('FilesAdded', function(up, files) {
            jQuery.each(files, function(i, file) {
                    jQuery('#filelist').append(
                            '<div id="' + file.id + '">' +
                            '<p style="float:none; width:inherit; max-height: 20px; overflow: hidden;">'+file.name + ' (' + plupload.formatSize(file.size) + ') ' +
                    '</p><b></b></div>');
            });

            up.refresh(); // Reposition Flash/Silverlight
    });

    uploader.bind('UploadProgress', function(up, file) {
            jQuery('#' + file.id + " b").html(file.percent + "%");
    });

    uploader.bind('Error', function(up, err) {
            jQuery('#filelist').append("<div>Error: " + err.code +
                    ", Message: " + err.message +
                    (err.file ? ", File: " + err.file.name : "") +
                    "</div>"
            );

            up.refresh(); 

    });

    uploader.bind('FileUploaded', function(up, file, response) {
        var server_response=response.response;
//        alert(server_response);
        var file=jQuery("#"+file.id);
        var extras=jQuery("<div></div>").appendTo(file);
        if (server_response.substr(0, 3)!='ok:')  {
    //                    jQuery("#"+file.id+" b").remove();
                jQuery(extras).css("color","#DE0000");
//                jQuery(file).css("font-size", '22px');
                jQuery('<p>'+server_response+" &#9785;</p>").appendTo(extras);
//                alert('hahaha');
            }
            else {
                var picture_path=server_response.substr(3);
//                jQuery(file).css("font-size", '22px');
                jQuery("<img src='<?php echo Helper::getServerName() ?>"+picture_path+"' style='width:301px; height:169px;' />").appendTo(extras);
                jQuery("<p style='font-size:23px;float:none;color:green;'>Successfully Uploaded &#9786;</p>").appendTo(extras);
                jQuery("<div><label style='font-size: 15px;' for='pic_thumbnail'>Thumbnail</label><input type='radio' name='pic_thumbnail' value='"+picture_path+"' /></div>").appendTo(extras);
                jQuery("<input type='hidden' value='"+picture_path+"' name='pic_path[]' />").appendTo(extras);
                jQuery("<input type='hidden' value='new' name='pic_state[]' />").appendTo(extras);
                jQuery("<input type='hidden' value='0' name='pic_id[]' />").appendTo(extras);
                jQuery("<h4 style='margin:0px'>Add some comments (optional):</h4>").appendTo(extras);
                jQuery("<textarea name='pic_comments[]' cols='40' rows='5'></textarea>").appendTo(extras);
                jQuery(file).addClass('uploaded_picture');
            }
//            alert(response.response);
        });

    uploader.refresh();
});
</script>
