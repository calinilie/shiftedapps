<?php 
    $path=dirname(dirname(__FILE__));
    require_once $path.'/Blocks/Block.php';
    require_once $path.'/Logic/Helper.php';
?>
<style>
    h3{
        display: block;
        margin:0px;
    }
    .clear{
        clear:both;
    }
    .left_side{
        width:16%;
        float:left;
        padding:20px 2%;
    }
    .right_side{
       width: 76%;
       float: left;
       padding: 20px 2%;
       max-width: 960px;
    }
    
    li{
        list-style: none;
        width:100%;
    }
    button{
        width:100%;
        margin:5px 0px;
        font-size: 16px;
        cursor: pointer;
    }
    fieldset{
        border:none;
    }
    .radio{
        float:left;
        width:15px;
    }
    p{
        margin:0px;
        float: left;
    }
    label{
        font-family: Arial;
        font-size: 20px;
    }
    .option{
        display: none;
        margin: 5px 0px;
        border-top: 2px solid greenyellow;
        padding: 5px 0px;
    }
    .active{
        border-color: greenyellow;
    }
    a{
        display: block;
    }
    .submit{
        font-size: 20px;
        color:green;
        font-weight: bold;
    }
    
    textarea{
        display: block;
    }
</style>
<script src="http://code.jquery.com/jquery-latest.js"></script>
<form action="<?php echo Helper::getServerName() ?>/admin.php" method="post">
    <div class="left_side">
        <li><button class="active" onclick="show(event, '#general_options', this)">General</button></li>
        <li><button onclick="show(event, '#paragraphs_list', this)">Paragraphs</button></li>
        <li><button onclick="show(event, '#pictures', this)">Pictures</button></li>
        <li><button onclick="show(event, '#videos', this)">Video</button></li>
        <input class="submit" type="submit" value="Submit" onclick="return validateArticle();"/>
    </div>
    <div class="right_side">
        <div class="option" id="general_options" style="display:block;">
            <h3>General options</h3>
            <fieldset>
                <div>
                    <label for="article_title">Title</label>
                    <input type='text' name="article_title" value="" size="50"/>
                    <div class="clear"></div>
                </div>
                <div class="categories">
                    <label for="catgeory">Category</label>
                    <div><input class="radio" type="radio" name="article_category" value="1"/><p>android</p></div>
                    <div class="clear"></div>
                    <div><input class="radio" type="radio" name="article_category" value="2"/><p>ios</p></div>
                    <div class="clear"></div>
                    <div><input class="radio" type="radio" name="article_category" value="3"/><p>about</p></div>
                    <div class="clear"></div>
                </div>
                <div>
                    <label for="article_featured">Featured</label>
                    <input type="checkbox" name="article_featured" />
                    <div class="clear"></div>
                </div>
                <div>
                    <label for="article_active">Active</label>
                    <input type="checkbox" name="article_active" />
                    <div class="clear"></div>
                </div>
                <div>
                    <label for="article_description">Article page Description</label>
                    <textarea name="article_description" cols="50" rows="5"></textarea>
                    <div class="clear"></div>
                </div>
            </fieldset>
        </div>
        <div class="option" id="paragraphs_list">
            <h3>Paragraphs</h3>
            <a id="add_paragraph" href="javascript:void(0)">+ Add Paragraph</a>
            <div class="paragraph_entry">
                <textarea class="paragraph_text" name="article_paragraphs[]" cols="111" rows="10"></textarea>
            </div>
            <div class="clear"></div>
        </div>
        <div class="option" id="pictures">
            <h3>Pictures</h3>
            <?php Block::renderBlock(Helper::getServerName()."/FrontEndAdmin/upload.php", NULL); ?>
            <div class="clear"></div>
        </div>
        <div class="option" id="videos">
            <h3>Videos</h3>
            <label for="video_link">Video Link</label>
            <input type="text" name="video_link" size="50"/>
            <h4 style='margin:0px'>Comments (optional):</h4>
            <textarea class="pic_comments" name='video_comments' cols='40' rows='5'></textarea>
            <div class="clear"></div>
        </div>
    </div>
    <input type="hidden" name="cmd" value="do_insert_article"/>
</form>
<script>
    $('#add_paragraph').click(function(){
       var newParagraph=$('<div class="paragraph_entry" style="display:none;"></div>');
       $('<textarea class="paragraph_text" name="article_paragraphs[]" cols="111" rows="10"></textarea>').appendTo(newParagraph);
       $('<a style="float:right;" href="javascript:void(0)" onclick="remove_this(this);">-Remove Paragraph</a>').appendTo(newParagraph);
       $('<div class="clear"></div>').appendTo(newParagraph);
       $(newParagraph).appendTo('#paragraphs_list');
       $(newParagraph).slideDown(200);
    });
    
    function remove_this(button){
        $(button).parent().remove();
        return false;
    }
    
    function show(event, target_id, button){
        event.preventDefault();
        $(target_id).slideToggle(200);
        $(button).toggleClass('active');
    };
</script>