<?php
$path=dirname(dirname(__FILE__));
$formats=array("image/jpeg","image/pjpeg","image/png","image/tiff");
if ($_FILES["file"]["size"] <= 10000000 && in_array($_FILES["file"]["type"], $formats)) {
    $filename = preg_replace('/[^\w\._]+/ ', '', $_FILES["file"]["name"]);
    if ($_FILES["file"]["error"] > 0) {
//        echo "Return Code: " . $_FILES["file"]["error"] . "<br />";
        echo "server error->tell Calin...";
    } else {
//        echo "Upload: " . $filename . "<br />";
//        echo "Type: " . $_FILES["file"]["type"] . "<br />";
//        echo "Size: " . ($_FILES["file"]["size"] / 1024) . " Kb<br />";
//        echo "Temp file: " . $_FILES["file"]["tmp_name"] . "<br />";

        if (file_exists($path."/picture_uploads/" . $filename)) {
            echo $filename. " already exists, rename it and try again";
        } else {
            if (!is_dir($path."/picture_uploads/")) {
                mkdir($path."/picture_uploads/");
            }
            move_uploaded_file($_FILES["file"]["tmp_name"], $path."/picture_uploads/" . $filename);
            echo 'ok:'."/picture_uploads/" . $filename;
        }
    }
}
else echo "too large(max size 10MB) or not allowed format (jpg, png or tiff)";
?>
