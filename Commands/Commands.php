<?
$path=dirname(dirname(__FILE__));
require_once $path.'/Commands/Command.php';
require_once $path.'/datasource/FacadeUser.php';
require_once $path.'/Logic/Helper.php';

class Commands{
    private $commands;
    private static $instance;
    
    private function __construct() {
        $this->commands=array();
        $this->commands['article']=new ArticleCommand();
        $this->commands['list']=new ListCommand();
        $this->commands['widgets']=new WidgetsCommand();
        $this->commands['featured']=new FeaturedCommand();
        $this->commands['loadMore']=new LoadMoreCommand();
    }
    
    public static function getInstance(){
        if (!isset (self::$instance))
                self::$instance=new Commands();
       return self::$instance;
    }
    
    public function executeCommand($cmd, $data){
        return $this->commands[$cmd]->execute($data);
    }
    public function render($cmd, $block){
        $this->commands[$cmd]->render($block);
    }
}

class ArticleCommand implements Command{
    //sertialized post request objects
    private $article;
    private $params_title_history;
    private $page_title_description;
    
    public function execute($data) {
        // REFACTOR ALL COMMANDS LIKE THIS. CALL THEM IN A BATCH REDUCE NO OF DB CALLS
        if (isset($data['article_id'])) $article_id=$data['article_id'];
        //else throw ArticleNotFoundException
        else $article_id=1;
        
        $article=FacadeUser::getInstance()->getFullArticle($article_id);
        $this->article=array('article'=>  serialize($article));
        $this->params_title_history=array('title'=> $article->getTitle(),
                                    'category' => $article->getCategory()->getName(),
                                    'category_url'=> Helper::genereateCategoryLink($article->getCategory()),
                                    'date_added' => $article->getAdded());
        $this->page_title_description=array('page_title'=>$article->getTitle(),
                                            'description'=>$article->getDescription());
        Helper::setCategoryId($article->getCategory()->getId());
    }
    
    public function render($block){
        switch ($block) {
            case 'article_block':
                Block::renderBlock(Helper::getServerName()."/FrontEndUsers/article.php", $this->article);
                break;
            case 'title_history':
                Block::renderBlock(Helper::getServerName()."/FrontEndUsers/title_history.php", $this->params_title_history);
                break;
            case 'title_description':
                Block::renderBlock(Helper::getServerName()."/FrontEndUsers/title_description.php", $this->page_title_description);
                break;
        }
    }
}

class ListCommand implements Command{
    private $articleDTOs;
    private $title_description;
    private $category_id;
    
    public function execute($data) {
        if (!isset($data['category'])) $category_url_identifier='home';
        else $category_url_identifier=$data['category'];

        if (!isset ($data['limit'])) $limit=7;
        else $limit=$data['limit'];
        
        //all articles - featured is used in featuredCommand!
        $featured=0;
        
        $category=FacadeUser::getInstance()->getCategory($category_url_identifier);
        $this->title_description=array("description"=> $category->getDescription(),
                                       "page_title"=> $category->getPageTitle()
                                        );
        
        $category_id=$category->getId();
        $articles=FacadeUser::getInstance()->getArticlesDTO($category_id, $limit, $featured);
        $this->articleDTOs=array('articles'=>serialize($articles),
                                 'category'=>$category_id,
                                 'lowerLimit'=>$limit);
        
        $this->category_id=array("category"=>$category_id);
        Helper::setCategoryId($category_id);
    }
    
    public function render($block){
        switch ($block) {
            case 'articles':
                Block::renderBlock(Helper::getServerName()."/FrontEndUsers/list.php", $this->articleDTOs);
                break;
            case 'title_description':
                Block::renderBlock(Helper::getServerName()."/FrontEndUsers/title_description.php", $this->title_description);
                break;
            case 'featured':
                Block::renderBlock(Helper::getServerName()."/FrontEndUsers/featured.php", $this->category_id);
                break;
        }
    }
    
    //mainly for testing purposes
    public function getArticleDTOs() {
        return $this->articleDTOs;
    }
    
    //mainly for testing purposes
    public function getTitle_description() {
        return $this->title_description;
    }
}

class FeaturedCommand implements Command{
    public function execute($data) {
        // REFACTOR liek ArticleCommand
        Block::renderBlock(Helper::getServerName()."/FrontEndUsers/featured.php", $data);
//        echo 'FEATURED COMMAND';
    }
}

class WidgetsCommand implements Command{
    public function execute($data) {
        Block::renderBlock(Helper::getServerName()."/FrontEndUsers/updates.php", null);
//        Block::renderBlock("http://".$_SERVER['SERVER_NAME']."/FrontEndUsers/newest_articles.php", null);
        Block::renderBlock(Helper::getServerName()."/FrontEndUsers/facebook_feed.php", null);
        Block::renderBlock(Helper::getServerName()."/FrontEndUsers/twitter_feed.php", null);
//        echo 'WIDGETS COMMAND';
    }
}    
class LoadMoreCommand implements Command{
    public function execute($data){
        $category_id=$data['category'];
        $limit=$data['limit'];
        $lowerLimit=$data['lower'];
        $articles=FacadeUser::getInstance()->loadMoreArticles($category_id, $lowerLimit, $limit);
        $result=array();
        foreach($articles as $article){
            $temp=array("id"=>$article->getId(),
                        "title"=>$article->getTitle(),
                        "paragraph"=>$article->getPreview(),
                        "urlIdentifier"=>$article->getUrlIdentifier(),
                        "added"=>$article->getAdded(),
                        "link"=>Helper::generateLinkFromArticleDTO($article));
            if ($article->hasThumbnail()) $temp['thumbnail']=Helper::getServerName().$article->getThumbnail()->getPath();
            array_push($result, $temp);
//            echo $article->getFirstParagraph()->getText();
        }
        $temp_result=array_values($result);
        return json_encode($temp_result);
    }
}

?>
