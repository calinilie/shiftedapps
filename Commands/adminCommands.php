<?php
$path=dirname(dirname(__FILE__));
require_once $path.'/Commands/Command.php';
require_once $path.'/Logic/AdminController.php';
require_once $path.'/Logic/Helper.php';
require_once $path.'/Blocks/Block.php';

class AdminCommands{
    private $commands;
    private static $instance;
    
    private function __construct() {
        $this->commands=array();
        $this->commands['do_insert_article']=new doInsertArticle();
        $this->commands['insert_article']=new insertArticle();
        $this->commands['update_article']=new updateArticle();
        $this->commands['do_update_article']=new doUpdateArticle();
        $this->commands['admin_auth']=new adminAuth();
        $this->commands['do_admin_auth']=new doAdminAuth();
        $this->commands['logout']=new logout();
    }
    
    public static function getInstance(){
        if (!isset (self::$instance))
                self::$instance=new AdminCommands();
       return self::$instance;
    }
    
    public function executeCommand($cmd, $data){
        $this->commands[$cmd]->execute($data);
    }
}

class insertArticle implements Command{
    public function execute($data) {
        Block::renderBlock(Helper::getServerName()."/FrontEndAdmin/NewArticle.php", NULL);
    }
}

class updateArticle implements Command{
    public function execute($data) {
        $controller=AdminController::getInstance();
        $controller->startUpdate($data['article_id']);
        $data['article']=serialize($controller->getArticle());
        Block::renderBlock(Helper::getServerName()."/FrontEndAdmin/EditArticle.php", $data);
//        foreach($data as $key=>$value){
//            echo $key.'=>'.$value.'<br />';
//        }
    }
}

class doInsertArticle implements Command{
    public function execute($data) {
        $controller=AdminController::getInstance();
        $article_title=$data['article_title'];
        $category=$data['article_category'];
        $featured=$data['article_featured']=="on" ? 1 : 0;
        $active=$data['article_active']=="on" ? 1 : 0;
        $description=$data['article_description'];
        $controller->startNew($article_title, $category, $featured, $active, $description);
        foreach($data["article_paragraphs"] as $text){
            $text=Helper::removeStupidCss($text);
            $controller->addParagraph($text);
        }
        
        $pics=$data['pic_path'];
        $pics_comments=$data['pic_comments'];
        $thumbnail=$data['pic_thumbnail'];
        $size=count($pics);
        for($i=0; $i<$size; $i++){
            $path=$pics[$i];
            $comments=$pics_comments[$i];
            echo $path.PHP_EOL.$thumbnail.PHP_EOL;
            $controller->addPhoto($path, $comments, $path==$thumbnail ? 1 : 0);
        }
        
        $link=$data['video_link'];
        $comments=$data['video_comments'];
        $controller->updateVideo(0, $link, $comments);
        
        $controller->save();
//        echo '<p style="color:green">article inserted, i hope...</p>';
        Reditect::redirect(Helper::getServerName()."/admin.php?message=insert_ok");
    }
}

class doUpdateArticle implements Command{
    public function execute($data) {
//        foreach($data as $name=>$value){
//            echo $name.'  ->  '.$value.'<br />';
//            if (is_array($value)){
//                foreach($value as $value_name){
//                    echo $value_name.'<br />';
//                }
//            }
//        }
        $controller=AdminController::getInstance();
        $article_state=$data['article_state'];
        if ($article_state=='changed'){
            $article_title=$data['article_title'];
            $article_category=$data['article_category'];
            $description=$data['article_description'];
            $controller->updateArticle($article_title, $article_category);
            $controller->updateArticleDescription($description);
            if (isset($data['article_featured'])){
                if ($data['article_featured']=='on')         
                    $controller->updateArticleFeatured(1);
            }
            else $controller->updateArticleFeatured(0);
            if (isset ($data['article_active'])){
                if ($data['article_active']=='on')
                $controller->updateArticleActive(1);
            }
            else $controller->updateArticleActive(0);
        }
        
        $paragraphs=$data['article_paragraphs'];
        $paragraphs_state=$data['article_paragraphs_state'];
        $paragraphs_ids=$data['article_paragraphs_ids'];
        $size=count($paragraphs);
        for($i=0; $i<$size; $i++){
            if ($paragraphs_state[$i]=="changed"){
                $paragraph_id=$paragraphs_ids[$i];
                $text=  Helper::removeStupidCss($paragraphs[$i]);
                $controller->editParagraph($paragraph_id, $text);
            }
            if($paragraphs_state[$i]=="new"){
                $text=  Helper::removeStupidCss($paragraphs[$i]);
                $controller->addParagraph($text);
            }
            if ($paragraphs_state[$i]=='delete'){
                $controller->deleteParagraph($paragraphs_ids[$i]);
            }
        }
        
        $pics=$data['pic_path'];
        $pics_ids=$data['pic_id'];
        $pics_state=$data['pic_state'];
        $pics_comments=$data['pic_comments'];
        $thumbnail=$data['pic_thumbnail'];
        $size=count($pics);
        for($i=0; $i<$size; $i++){
            if ($pics_state[$i]=='new'){
                $path=$pics[$i];
                $comments=$pics_comments[$i];
                $controller->addPhoto($path, $comments, $path==$thumbnail? 1 : 0);
            }
            if ($pics_state[$i]=='changed'){
                $picture_id=$pics_ids[$i];
                $comments=$pics_comments[$i];
                $path=$pics[$i];
                $controller->editPhoto($picture_id, $comments, $path==$thumbnail ? 1 : 0);
            }
            if ($pics_state[$i]=='delete'){
                $picture_id=$pics_ids[$i];
                $controller->deletePhoto($picture_id);
            }
        }
        
        if($data['video_state']=='changed'){
            $video_id=$data['video_id'];
            $link=$data['video_link'];
            $comments=$data['video_comments'];
            $controller->updateVideo($video_id, $link, $comments);
        }
        
        $controller->save();
        Reditect::redirect(Helper::getServerName()."/admin.php?message=update_ok");
    }
}

class adminAuth implements Command{
    public function execute($data) {
        Block::renderBlock(Helper::getServerName()."/FrontEndAdmin/auth.php", NULL);
    }
}

class doAdminAuth implements Command{
    public function execute($data) {
        if (isset ($data['username'])&& isset ($data['password'])){
            $username=trim($data['username']);
            $password=$data['password'];
            if (AdminController::getInstance()->adminAuth($username, $password)==true){
//                Block::renderBlock(Helper::getServerName()."/FrontEndAdmin/list.php", NULL);
                Reditect::redirect(Helper::getServerName().'/admin.php');
                ob_flush();
            }
            else echo 'invalid username or password';
        }
        else echo 'invalid username or password';
    }
}

class logout implements Command{
    public function execute($data) {
        AdminController::getInstance()->logout();
        Block::renderBlock(Helper::getServerName()."/FrontEndAdmin/auth.php", NULL);
    }
}
?>
