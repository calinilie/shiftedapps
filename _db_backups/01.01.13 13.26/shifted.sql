-- MySQL dump 10.11
--
-- Host: n1grid50mysql107.secureserver.net    Database: shifted
-- ------------------------------------------------------
-- Server version	5.0.92-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `articles`
--

DROP TABLE IF EXISTS `articles`;
CREATE TABLE `articles` (
  `article_id` int(11) NOT NULL auto_increment,
  `title` varchar(200) NOT NULL,
  `category` int(1) default NULL,
  `added` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `featured` tinyint(1) NOT NULL default '0',
  `url_identifier` varchar(200) NOT NULL,
  `active` tinyint(1) NOT NULL default '0',
  PRIMARY KEY  (`article_id`),
  UNIQUE KEY `url_identifier` (`url_identifier`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `articles`
--

LOCK TABLES `articles` WRITE;
/*!40000 ALTER TABLE `articles` DISABLE KEYS */;
INSERT INTO `articles` VALUES (1,'Featured Android Article no1  ',1,'2011-11-09 23:05:55',1,'featured-android-article-no1',1),(2,'Android Article no2',1,'2011-11-09 23:05:55',0,'android-article-no2',1),(3,'iOS article no3',2,'2011-11-09 23:32:11',0,'ios-article-no3',1),(4,'Featured iOS Article no 4',2,'2012-01-19 21:11:47',1,'featured-ios-article-no4',1),(5,'article no 5 ',1,'2012-01-25 19:51:22',0,'article-no5',1),(6,'Article Inserted from App',1,'2012-01-25 20:11:50',0,'article-inserted-from-app',1),(16,'from android to ios',2,'2012-01-29 09:23:15',1,'this-article-was-added-when-developing-the-uow',1),(17,'calin edited this shit!',2,'2012-01-30 07:17:01',0,'asda',1),(18,'testing admin part on shiftedapps',2,'2012-01-30 07:18:15',1,'second-try',1),(19,'from browser, trying',1,'2012-01-31 08:32:22',1,'from-browser-trying',1),(20,'trying to update',1,'2012-01-31 09:34:07',1,'blablablabla',1),(21,'this article was added from the browser admin part',1,'2012-02-03 23:58:44',1,'this-article-was-added-from-the-browsers-admin-part',1),(22,'dsadsadsa',2,'2012-02-05 01:32:48',1,'dsadsadsa',1),(23,'Mike, cand vezi asta sa dai si tu un semn...',1,'2012-02-05 10:26:24',1,'ndklsakldmslal',1),(24,'article with pictures....',1,'2012-02-07 00:56:17',0,'article-with-pictures',1),(25,'dsads',1,'2012-02-07 01:01:33',0,'dsads',1),(26,'dsadsadsadsadsadsa',1,'2012-02-07 01:04:01',0,'dsadsadsadsadsadsa',1),(27,'12321321321',2,'2012-02-07 01:08:17',1,'12321321321',1),(28,'ggggggg123232',1,'2012-02-07 01:11:10',1,'ggggggg',1),(29,'testing stufffdfd',1,'2012-02-07 01:12:38',1,'5435433dssfdg',1),(30,'inactive article',1,'2012-02-11 00:10:21',0,'inactive-article',1),(31,'new inactive article',1,'2012-02-11 00:39:03',1,'new-inactive-article',1),(32,'with video',1,'2012-02-12 22:52:00',0,'with-video',1),(33,'bla bla bla',1,'2012-02-12 22:54:10',1,'bla-bla-bla',1),(34,'Lorem Ipsum',2,'2012-02-19 19:19:16',0,'lorem-ipsum',1),(35,'Man survived for two months buried in snow',2,'2012-02-20 22:47:22',1,'man-survived-for-two-months-buried-in-snow',1),(36,'Cow Copter',1,'2012-02-20 23:45:24',1,'cow-copter',1);
/*!40000 ALTER TABLE `articles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `articles_users`
--

DROP TABLE IF EXISTS `articles_users`;
CREATE TABLE `articles_users` (
  `article_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  KEY `article_id` (`article_id`,`user_id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `articles_users_ibfk_1` FOREIGN KEY (`article_id`) REFERENCES `articles` (`article_id`),
  CONSTRAINT `articles_users_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `articles_users`
--

LOCK TABLES `articles_users` WRITE;
/*!40000 ALTER TABLE `articles_users` DISABLE KEYS */;
/*!40000 ALTER TABLE `articles_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `credentials`
--

DROP TABLE IF EXISTS `credentials`;
CREATE TABLE `credentials` (
  `user_id` int(11) NOT NULL,
  `username` varchar(30) NOT NULL,
  `pass` varchar(30) NOT NULL,
  UNIQUE KEY `user_id` (`user_id`),
  CONSTRAINT `credentials_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `credentials`
--

LOCK TABLES `credentials` WRITE;
/*!40000 ALTER TABLE `credentials` DISABLE KEYS */;
INSERT INTO `credentials` VALUES (1,'shifteduser','YQC!zb7FoIhS3cmh');
/*!40000 ALTER TABLE `credentials` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `logins`
--

DROP TABLE IF EXISTS `logins`;
CREATE TABLE `logins` (
  `user_id` int(11) NOT NULL,
  `time_stamp` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `logged_on` tinyint(1) NOT NULL default '0',
  `last_activity` bigint(111) NOT NULL,
  UNIQUE KEY `time_stamp` (`time_stamp`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `logins_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `logins`
--

LOCK TABLES `logins` WRITE;
/*!40000 ALTER TABLE `logins` DISABLE KEYS */;
/*!40000 ALTER TABLE `logins` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `paragraphs`
--

DROP TABLE IF EXISTS `paragraphs`;
CREATE TABLE `paragraphs` (
  `paragraph_id` int(11) NOT NULL auto_increment,
  `article_id` int(11) NOT NULL,
  `text` longtext NOT NULL,
  `added` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  PRIMARY KEY  (`paragraph_id`),
  KEY `article_id` (`article_id`),
  CONSTRAINT `paragraphs_ibfk_1` FOREIGN KEY (`article_id`) REFERENCES `articles` (`article_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=75 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `paragraphs`
--

LOCK TABLES `paragraphs` WRITE;
/*!40000 ALTER TABLE `paragraphs` DISABLE KEYS */;
INSERT INTO `paragraphs` VALUES (2,2,'<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>','2012-02-18 20:46:34'),(3,1,'<p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>','2012-02-12 23:50:01'),(4,1,'<p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. </p>','2012-02-12 23:50:01'),(5,1,'<p>Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. </p>','2012-02-12 23:50:01'),(6,2,'<p>Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores.</p>','2012-02-18 20:46:34'),(7,3,'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. ','2011-11-14 02:08:20'),(8,2,'<p>Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?</p>','2012-02-18 20:46:34'),(9,6,'some text some text some text some text some text some text some text some text some text some text some text some text some text some text some text some text some text some text some text some text some text some text some text some text ','2012-01-29 00:15:43'),(10,1,'<p></p><div style=\"text-align: center;\">adsadsadsad</div><div style=\"text-align: center;\"><strike>sadsadsa</strike></div><div style=\"text-align: center;\"><u style=\"text-decoration: line-through; \">dsadsadsadsas</u><sup>2</sup></div><div style=\"text-align: center;\"><a href=\"http://shiftedapps.com\">dsadsadsadsa</a></div><p></p>','2012-02-12 23:50:01'),(28,16,'shitty article','2012-01-29 09:23:15'),(32,17,'d','2012-02-03 21:54:14'),(33,18,'added admin part to this webapp','2012-02-04 23:20:58'),(34,18,'testing it now.... hope it works','2012-02-04 23:20:58'),(35,18,'Dasdsadsa from page 2','2012-01-30 07:18:15'),(36,19,'some paragraph','2012-01-31 08:32:22'),(37,20,'update paragraph','2012-02-03 21:40:57'),(38,16,'this paragraph was added fom the browser','2012-02-03 23:43:55'),(39,21,'paragraph 1','2012-02-03 23:58:44'),(40,21,'paragraph 2','2012-02-03 23:58:44'),(41,16,'this paragraph was added at around 2:15 am Sunday morning','2012-02-05 01:15:26'),(43,22,'dsadsadsa','2012-02-05 01:32:48'),(44,23,'makldmkslaldsa','2012-02-05 10:26:24'),(45,23,'dasdsadsadasdsadsads','2012-02-05 10:26:24'),(46,23,'adsadsadsad','2012-02-05 10:26:24'),(47,23,'sadsadsa','2012-02-05 10:26:24'),(48,24,'dsadsa','2012-02-07 00:56:17'),(49,25,'dsadsa','2012-02-07 01:01:33'),(50,26,'dsadsa','2012-02-07 01:04:01'),(51,27,'132132121321','2012-02-07 01:08:17'),(52,28,'<p style=\"text-align: left;\"><u>akdsadklsad klsamdskladmsakldmsa kmdkasmdlasdsa</u></p><p><br></p>','2012-02-20 22:51:39'),(63,29,'<h1>stuff<br><div style=\"text-align: center;\">stuff</div><div style=\"text-align: right;\">stuff</div><div style=\"text-align: center;\">stuff</div>stuff</h1>','2012-02-10 02:13:40'),(64,30,'<p>inactive article inactive article inactive article inactive article inactive article inactive article inactive article inactive article inactive article </p>','2012-02-11 00:28:32'),(65,31,'<p></p><h1 style=\"text-align: center;\">stuff inactive article</h1><p></p>','2012-02-11 00:39:21'),(66,32,'<p>asdasdsadsa</p>','2012-02-12 23:18:45'),(69,33,'<p></p><div style=\"text-align: center;\">ljkjjkjjkjkjl</div><div style=\"text-align: center;\"><u>kkjkjkjkjjkjljkjl</u></div><p></p>','2012-02-17 12:47:05'),(70,1,'<h1><i style=\"font-weight: normal;\">some paragraph</i></h1>','2012-02-19 18:49:30'),(71,34,'<p><span style=\"text-align: justify; \">updated now ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, adsquis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cisdadsallum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</span></p>','2012-02-23 01:37:36'),(73,35,'<p style=\"text-align: left; \"><u>Breaking NEWS</u>&nbsp;</p><p>A man from&nbsp;Sweden&nbsp;survived for 2&nbsp;<b>MONTHS&nbsp;</b>with no food at&nbsp;temperatures&nbsp;reaching under -30<sup>o</sup>C</p><p style=\"text-align: right; \">Source:&nbsp;<a href=\"http://gizmo.do/AAYskL\">Gizmodo</a></p>','2012-02-20 22:47:22'),(74,36,'<p>I was browsing around <a href=\"http://bit.ly/yndPDi\">YoYo Games</a>&nbsp;and amongst many of their publications I downloaded CowCopter (the free version), one of their Android titles.<br></p><div>Honestly, it was a pleasant surprise the way it ran on my Samsung Galaxy Y, one of the low-end Android terminals on the market. At a first glance (5 min playtime), it seemed quite a fun, addictive game.&nbsp;</div><div><br></div><div>CowCopter features, as the title suggests, a female bull with a propeller sticking out its butt, who (i guess it\'s alright to call it who??) &nbsp;is flying around, trying to collect stars. I have only managed to discover two reasons for that - there might be more - but lets take it slowly:<br></div><div><ul><li>first reason: level UP - once you do, you unlock other little living things in this world, which so far i have no&nbsp;idea&nbsp;what they are for....</li><li>second reason: keep flying - the time span in which one can actually fly is&nbsp;obviously&nbsp;limited and collecting start kinda feeds up fuel to be ably to run that&nbsp;propeller from your behind.&nbsp;</li></ul><div>Haven\'t played much, but it seems fun, so I will keep leveling up and I will keep you guys up to date, so stick around for more info.&nbsp;</div><div><br></div><div>Until next time, behave!</div></div><div><br></div><div><a href=\"http://bit.ly/xYsoPs\">CowCopter Lite on Android Market</a></div><p></p>','2012-02-21 00:09:07');
/*!40000 ALTER TABLE `paragraphs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pics`
--

DROP TABLE IF EXISTS `pics`;
CREATE TABLE `pics` (
  `pic_id` int(11) NOT NULL auto_increment,
  `article_id` int(11) NOT NULL,
  `path` longtext NOT NULL,
  `comments` varchar(400) default NULL,
  `added` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  `thumbnail` tinyint(1) NOT NULL default '0',
  PRIMARY KEY  (`pic_id`),
  KEY `article_id` (`article_id`),
  CONSTRAINT `pics_ibfk_1` FOREIGN KEY (`article_id`) REFERENCES `articles` (`article_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pics`
--

LOCK TABLES `pics` WRITE;
/*!40000 ALTER TABLE `pics` DISABLE KEYS */;
INSERT INTO `pics` VALUES (1,1,'/picture_uploads/test.png','This picture is used for testing purposes','2012-02-08 22:33:42',0),(2,1,'/picture_uploads/test.png','This picture is used for testing purposes','2012-02-08 22:33:42',0),(3,4,'/picture_uploads/test.png','This picture is used for testing purposes','2012-02-08 22:33:42',0),(4,6,'/picture_uploads/test.png','This picture is used for testing purposes','2012-02-08 22:33:42',0),(5,1,'/picture_uploads/test.png','This picture is used for testing purposes','2012-02-08 22:33:42',0),(9,16,'/picture_uploads/test.png','This picture is used for testing purposes','2012-02-08 22:33:42',0),(13,28,'/picture_uploads/cil_twitter.png','some comments of awesomeness :Ds','2012-02-18 20:31:51',1),(14,29,'/picture_uploads/388552_218123621604129_128697780546714_496913_919720896_n.jpg','cool, ha?','2012-02-08 22:54:41',1),(15,27,'/picture_uploads/test.png','','2012-02-08 22:56:21',1),(22,29,'/picture_uploads/IMG_5193.JPG','lyngby lake','2012-02-09 18:47:03',1),(23,31,'/picture_uploads/Submitknap.jpg','submit button','2012-02-11 00:43:56',1),(26,28,'/picture_uploads/picturesShiftedApps1.png','some comments of more awsomeness','2012-02-20 22:09:49',0),(27,35,'/picture_uploads/buried_under_snow.png','','2012-02-20 22:47:22',0),(28,36,'/picture_uploads/cow_copter.png','','2012-02-20 23:46:52',1);
/*!40000 ALTER TABLE `pics` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `user_id` int(11) NOT NULL auto_increment,
  `name` varchar(30) default NULL,
  `description` varchar(300) default NULL,
  `pic` varchar(300) default NULL,
  PRIMARY KEY  (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'default','default_user',NULL);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vids`
--

DROP TABLE IF EXISTS `vids`;
CREATE TABLE `vids` (
  `vid_id` int(11) NOT NULL auto_increment,
  `article_id` int(11) NOT NULL,
  `link` varchar(300) NOT NULL,
  `comments` varchar(300) default NULL,
  PRIMARY KEY  (`vid_id`),
  KEY `article_id` (`article_id`),
  CONSTRAINT `vids_ibfk_1` FOREIGN KEY (`article_id`) REFERENCES `articles` (`article_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vids`
--

LOCK TABLES `vids` WRITE;
/*!40000 ALTER TABLE `vids` DISABLE KEYS */;
INSERT INTO `vids` VALUES (1,1,'http://youtu.be/V6T6ue3mQ58','some comments'),(2,2,'http://youtu.be/V6T6ue3mQ58',NULL),(3,32,'http://youtu.be/V6T6ue3mQ58','some commets'),(4,33,'http://youtu.be/V6T6ue3mQ58','jslkdasdjklsadjsla1'),(5,34,'http://youtu.be/V6T6ue3mQ58',''),(7,36,'','');
/*!40000 ALTER TABLE `vids` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2012-02-25 17:48:08
