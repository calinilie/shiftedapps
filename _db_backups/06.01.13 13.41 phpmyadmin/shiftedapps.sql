-- phpMyAdmin SQL Dump
-- version 2.11.11.3
-- http://www.phpmyadmin.net
--
-- Host: 188.121.40.80
-- Generation Time: Jan 06, 2013 at 05:39 AM
-- Server version: 5.0.96
-- PHP Version: 5.3.2

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

--
-- Database: `shiftedapps`
--

-- --------------------------------------------------------

--
-- Table structure for table `articles`
--

DROP TABLE IF EXISTS `articles`;
CREATE TABLE IF NOT EXISTS `articles` (
  `article_id` int(11) NOT NULL auto_increment,
  `title` varchar(200) NOT NULL,
  `added` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `featured` tinyint(1) NOT NULL default '0',
  `url_identifier` varchar(200) NOT NULL,
  `active` tinyint(1) NOT NULL default '0',
  `description` varchar(500) NOT NULL,
  PRIMARY KEY  (`article_id`),
  UNIQUE KEY `url_identifier` (`url_identifier`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=306 ;

--
-- Dumping data for table `articles`
--

INSERT INTO `articles` VALUES(36, 'Cow Copter', '2012-02-20 16:45:24', 1, 'cow-copter', 1, 'Cow Copter is a simple yet challenging game that runs smoothly on low-end Android devices as well. Cow Copter, a nicely done little game is a lot of fun!');
INSERT INTO `articles` VALUES(280, 'The Team', '2012-03-25 00:23:13', 0, 'the-team', 1, 'Roxana Andronic, Mihai Moraru and Calin Laurentiu Ilie form the team that brings you the greatest apps and games from the google play market and the app store; and we don''t stop here!');
INSERT INTO `articles` VALUES(281, 'Contact', '2012-03-25 00:38:44', 0, 'contact', 1, 'Contact us to let us know what\\''s up, or what you think.');
INSERT INTO `articles` VALUES(293, 'Noogra Nuts on  iOS', '2012-03-29 05:34:17', 1, 'noogra-nuts-on--ios', 1, 'Noogra Nuts finally on iOS. Nugra Nuts features a cute squirrel, your goal is to collect as many points as possible by cracking and eating nuts.  ');
INSERT INTO `articles` VALUES(294, 'The Hunger Games: Girl on Fire', '2012-03-31 08:02:03', 1, 'the-hunger-games-girl-on-fire', 1, 'The Hunger Games: Girl on fire , is the official free game for the movie the Hunger Games. You need to defend Katniss against the capitol and help her survive the evil Traker Jackers.');
INSERT INTO `articles` VALUES(297, 'Shane Reaction: ZOMBIE DASH', '2012-04-11 04:56:03', 1, 'shane-reaction', 1, 'Shane Reaction is a fun, simple android game where you have to kill zombies in order to fight for your life and hopefully find your beloved lady. ');
INSERT INTO `articles` VALUES(298, 'Panda Run, an Android clone of Temple Run', '2012-04-14 07:20:53', 1, 'panda-run', 1, 'Panda Run is a Temple Run Replica, with happier colors, a funny, although overweight panda. It runs on Android lower-end devices as well.');
INSERT INTO `articles` VALUES(299, 'Pet Dash', '2012-04-17 08:06:13', 1, 'pet-dash', 1, 'Pet dash features kittens, bunnies. It is a fun stimulating android game.');
INSERT INTO `articles` VALUES(300, 'Why is everyone against Google?', '2012-04-18 02:34:42', 0, 'why-is-everyone-against-google', 1, 'Oracle amongst many others are suing Google. Why is everybody suing Google?');
INSERT INTO `articles` VALUES(301, 'Monkey Flight Review', '2012-04-18 04:28:31', 1, 'monkey-flight', 1, 'Monkey Flight is an iOS game, featuring a monkey. Your goal is to collect a certain number of fruits to pass every level. ');
INSERT INTO `articles` VALUES(302, 'What we do', '2012-04-18 05:51:03', 0, 'what-we-do', 1, 'We write our own opinions, as app consumers, about the best titles on Google Play and iOS App Store.');
INSERT INTO `articles` VALUES(303, 'Android &#34Holo&#34 design language to be mandatory', '2012-04-22 11:11:52', 1, 'android-holo-design-language-to-be-mandatory', 1, 'Holo themed apps. Android &#34Holo&#34 design language to be mandatory.');
INSERT INTO `articles` VALUES(304, 'testing some shit', '2012-04-22 12:24:54', 0, 'testing-some-shit', 0, 'desc');
INSERT INTO `articles` VALUES(305, '8bit Ninja Review', '2012-04-26 10:00:54', 0, 'bit-ninja', 1, '8bit Ninja is a retro, simple game brought to you by  Dogbyte Games. It features a ninja, in a 8bit world, and all you have to do is to move around in order to avoid the falling fruits. Awesome game review for ninja 8bit.');

-- --------------------------------------------------------

--
-- Table structure for table `articles_users`
--

DROP TABLE IF EXISTS `articles_users`;
CREATE TABLE IF NOT EXISTS `articles_users` (
  `article_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  KEY `article_id` (`article_id`,`user_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `articles_users`
--


-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
CREATE TABLE IF NOT EXISTS `categories` (
  `category_id` int(11) NOT NULL auto_increment,
  `name` varchar(100) NOT NULL,
  `url_identifier` varchar(100) NOT NULL,
  `description` varchar(500) NOT NULL,
  `parent_category` int(11) default NULL,
  `page_title` varchar(100) default NULL,
  PRIMARY KEY  (`category_id`),
  UNIQUE KEY `url_identifier` (`url_identifier`),
  KEY `parent_category` (`parent_category`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` VALUES(0, 'home', 'home', 'Mainly mobile apps reviews on both android and ios platforms, giving our own opinion as app consumers. Tech blog. ', NULL, 'Shifted Apps - A world of apps');
INSERT INTO `categories` VALUES(1, 'Android', 'android', 'android description', NULL, 'android page title');
INSERT INTO `categories` VALUES(2, 'iOS', 'ios', 'ios description', NULL, 'ios page title');
INSERT INTO `categories` VALUES(3, 'About', 'about', 'All you need to know about Shifted Apps, our goals and the team. We write about apps and games we like.', NULL, 'About');

-- --------------------------------------------------------

--
-- Table structure for table `categories_articles`
--

DROP TABLE IF EXISTS `categories_articles`;
CREATE TABLE IF NOT EXISTS `categories_articles` (
  `article_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  KEY `category_id` (`category_id`),
  KEY `article_id` (`article_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `categories_articles`
--

INSERT INTO `categories_articles` VALUES(36, 1);
INSERT INTO `categories_articles` VALUES(280, 3);
INSERT INTO `categories_articles` VALUES(281, 3);
INSERT INTO `categories_articles` VALUES(293, 2);
INSERT INTO `categories_articles` VALUES(294, 2);
INSERT INTO `categories_articles` VALUES(297, 1);
INSERT INTO `categories_articles` VALUES(298, 1);
INSERT INTO `categories_articles` VALUES(299, 1);
INSERT INTO `categories_articles` VALUES(300, 1);
INSERT INTO `categories_articles` VALUES(301, 2);
INSERT INTO `categories_articles` VALUES(302, 3);
INSERT INTO `categories_articles` VALUES(303, 1);
INSERT INTO `categories_articles` VALUES(304, 2);
INSERT INTO `categories_articles` VALUES(305, 2);

-- --------------------------------------------------------

--
-- Table structure for table `credentials`
--

DROP TABLE IF EXISTS `credentials`;
CREATE TABLE IF NOT EXISTS `credentials` (
  `user_id` int(11) NOT NULL,
  `username` varchar(30) NOT NULL,
  `pass` varchar(30) NOT NULL,
  UNIQUE KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `credentials`
--

INSERT INTO `credentials` VALUES(1, 'shifteduser', 'YQC!zb7FoIhS3cmh');

-- --------------------------------------------------------

--
-- Table structure for table `logins`
--

DROP TABLE IF EXISTS `logins`;
CREATE TABLE IF NOT EXISTS `logins` (
  `user_id` int(11) NOT NULL,
  `time_stamp` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `logged_on` tinyint(1) NOT NULL default '0',
  `last_activity` bigint(111) NOT NULL,
  UNIQUE KEY `time_stamp` (`time_stamp`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `logins`
--


-- --------------------------------------------------------

--
-- Table structure for table `paragraphs`
--

DROP TABLE IF EXISTS `paragraphs`;
CREATE TABLE IF NOT EXISTS `paragraphs` (
  `paragraph_id` int(11) NOT NULL auto_increment,
  `article_id` int(11) NOT NULL,
  `text` longtext NOT NULL,
  `added` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  PRIMARY KEY  (`paragraph_id`),
  KEY `article_id` (`article_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=665 ;

--
-- Dumping data for table `paragraphs`
--

INSERT INTO `paragraphs` VALUES(74, 36, '<p>I was browsing around <a href="http://bit.ly/yndPDi">YoYo Games</a>&nbsp;and amongst many of their publications I downloaded CowCopter (the free version), one of their Android titles.<br></p><div>Honestly, it was a pleasant surprise the way it ran on my Samsung Galaxy Y, one of the low-end Android terminals on the market. At a first glance (5 min playtime), it seemed quite a fun, addictive game.&nbsp;</div><div><br></div><div>CowCopter features, as the title suggests, a female bull with a propeller sticking out its butt, who (i guess it''s alright to call it who??) &nbsp;is flying around, trying to collect stars. I have only managed to discover two reasons for that - there might be more - but lets take it slowly:<br></div><div><ul><li>first reason: level UP - once you do, you unlock other little living things in this world, which so far i have no&nbsp;idea&nbsp;what they are for....</li><li>second reason: keep flying - the time span in which one can actually fly is&nbsp;obviously&nbsp;limited and collecting start kinda feeds up fuel to be ably to run that&nbsp;propeller from your behind.&nbsp;</li></ul><div>Haven''t played much, but it seems fun, so I will keep leveling up and I will keep you guys up to date, so stick around for more info.&nbsp;</div><div><br></div><div>Until next time, behave!</div></div><div><br></div><div><a href="http://bit.ly/xYsoPs">CowCopter Lite on Android Market</a></div><p></p>', '2012-02-20 17:09:07');
INSERT INTO `paragraphs` VALUES(637, 280, 'Roxana is a passionate marketeer, focusing on effective communication through social media. She''s a little bit obsessed with Facebook and Pinterest. Besides that, she''s an animal lover and a future shopaholic.', '2012-04-18 06:26:45');
INSERT INTO `paragraphs` VALUES(638, 280, 'Mihai Moraru should write some thing here', '2012-03-26 12:51:20');
INSERT INTO `paragraphs` VALUES(639, 280, 'Calin Laurentiu Ilie ', '2012-04-18 04:38:19');
INSERT INTO `paragraphs` VALUES(640, 281, 'You can mail us to <a href="mailto:mail@shiftedapps.com">this address</a> if you would like to tell us what is bothering you. \r\nOops.., maybe we did not mention, but you we also work as shrinks for people with various problems, for instance:\r\n<ul>\r\n<li>you have too much money</li>\r\n<li>you have a good idea and too much money</li>\r\n<li>or you simply need an energetic, capable, enthusiastic development team in order to build your website, mobile application or game</li>\r\n</ul>\r\nDrop us a line, tell us what you think! We are open to suggestions and/or criticism, but keep it nice ;)', '2012-03-25 11:41:26');
INSERT INTO `paragraphs` VALUES(648, 293, 'I was really looking forward for Noogra Nuts to be available for iOS. So, I was really, really excited when I could finally download it. \r\n<br />\r\n<br />\r\nNoogra Nuts, is a fun little game, featuring a cute squirrel. She has to crack these nuts falling from the sky, two-three times until she can eat the nut inside. The game has three modes : classic, jungle and survival.\r\nIn the classic mode, you have 2 minutes to crack as many nuts as possible in order to get a higher score. Some kinds of nuts have a 15 points value, some worth 200 points. In the Jungle mode, you will have also 2 minutes to collect as many points as possible. The difference here is that if we eat some of the "bad" nuts, you will lose 100 points. Also, big nuts might be rolling on the ground, so you will have to jump over them, in order to survive.\r\n<br /> \r\n<br />\r\nThe second and last mode is Survival. Here you don''t have a limit of time. You can play as long as you want. And as long as you don''t lose all of your three lives. The trick here is not to get killed by the big rocks falling from the sky, or from the tumbleweed.\r\n<br/>\r\n<br />\r\nI really wish that they will have some in app purchases in the future (like in the android version), because it felt nice to earn coins after the classical mode and then go buy the little squirrel a funny hat. All in all, I like the simplicity of this game. You don''t need mad skills to play Noogra Nuts; you just have to tilt your phone to make her move to one side or the other and tap the screen to jump\r\n<br />\r\n<br />\r\n\r\nHope you enjoy this game, and let me know what is your highest score. :)\r\n', '2012-04-09 08:10:22');
INSERT INTO `paragraphs` VALUES(649, 294, 'I really liked the Hunger Games movie for a few reasons. First, it was awesome and second, the marketing campaign behind it was pretty extraordinary too. The marketing team spent only 4 million dollars to promote the movie, because they used inexpensive ways of advertising such as Facebook or Twitter.\r\n<br />\r\nAnyways, I believe that in their efforts of making The Hunger Games viral, they also released The Hunger Games : Girl on fire game in the App Store. So, if you liked the movie or the books, or you simply want to enjoy a cool, free game, this might be a good choice.\r\n<br />\r\n<br />\r\nThe gameplay it''s not too complicated. You''re playing as the courageous Katniss Evergreen, and you need to survive in the woods beyond district 12 or later, to find your way back to district 12. The game is an auto runner game, and you need to kill the scary evil wasps that follow you and to avoid the hallucinogenic substance that they emanate. The good news is that you have unlimited arrows :). Every Time a Tracker Jacker touches you, you''ll see the screen pulsating, but you won''t die unless they touch you again while the screen is pulsating. \r\n<br />\r\n<br />\r\nOther than killing the wasps, you can also avoid them by swiping up to go up in the trees or swiping down to go back to the ground. \r\n<br />\r\n<br />\r\nThis being said, I really like this game. I like the sound effect and the old school way that it looks. I also enjoyed the small details like the MockingJay symbol at the beginning. \r\n\r\nI really hope that you enjoy this little free game,and <em> may the odds be ever in your favor </em>. :)\r\n<br />\r\n<br />\r\n<a href="http://itunes.apple.com/us/app/hunger-games-girl-on-fire/id512146822?mt=8" alt="The Hunger Games on iOS" title="The Hunger Games on iOS">The Hunger Games : Girl on Fire on App Store</a>', '2012-04-02 07:57:07');
INSERT INTO `paragraphs` VALUES(653, 297, 'I was just looking for a new, interesting, fun, simple game which might actually be tough to mix in a mobile game. Luckily though, the team at <a href="http://artbitgames.com/">ArtBit Games</a> has managed to do just that with Shane Reaction, one of their Android titles.\r\n<br />\r\n<br />\r\nThe gameplay is not that complex, you just have to tap the screen in order to shoot zombies coming towards Shane - the main caracter. You earn coins (which have to be picked up - might not sound like a big deal, but it makes the game so much more challenging), coins that are used to buy and/or upgrade weapons and defenses - pretty straight-forward! A simple RPG for mobile that my grandma can actually play, although I think the zombies might freak her out!\r\n<br />\r\n<br />\r\nAnother interesting feature about this game is the story line and the way it is narrated. The story starts in the "future" when Shane gets his ass raped (by that i mean killed) by a bunch of nasty zombies, then the action shifts to "40 days earlier" when you get to start playing. It does not stop here; there are levels (I have encountered only one so far, but I suppose there might be more) where the actions represents Shane''s memories. These levels might not be very challenging but they do provide great clues to the story. SPOILER ALERT - at one point Shane says "If I am alive, then she must be too", so probably he is fighting all the smelly bastards to find and get reunited with his Love - isn''t that cute? \r\n<br />\r\n<br />\r\nAnyways guys, this is a fun game, I recommend that you give it a try.<br />\r\n<a href="https://play.google.com/store/apps/details?id=com.artbit.shanereactionfree#?t=W251bGwsMSwxLDIxMiwiY29tLmFydGJpdC5zaGFuZXJlYWN0aW9uZnJlZSJd">Shane Reaction: ZOMBIE DASH on Google Play </a>\r\n<br />\r\nUntil next time, remember to support developers!', '2012-04-11 06:32:27');
INSERT INTO `paragraphs` VALUES(654, 298, 'I have a lot of trouble finding good games for my <a href="http://www.gsmarena.com/samsung_galaxy_y_s5360-4117.php">Samsung Galaxy Y</a> since it is one of the low-end Android terminals, so I was really surprised when I downloaded Panda Run, as it runs pretty smoothly on my Samsung. The game has nice 3D graphics, probably not as textured as <a href="https://play.google.com/store/apps/details?id=com.imangi.templerun&feature=search_result#?t=W251bGwsMSwxLDEsImNvbS5pbWFuZ2kudGVtcGxlcnVuIl0.">Temple Run</a>, but definitely more colorful. Even if it is kinda the exact replica of Temple Run i still think it is worth giving it a shot.\r\n<br />\r\nI would probably choose Temple Run over this one, because there are more active players that I would be able to compete with,  the characters on TR seem to move in a more natural way, and honestly they are better looking. I mean, seriously who would choose a fat panda over an animated Lara Croft?\r\n<br /><br />\r\nIf you like the panda, give it a try, it is quite nice.\r\n<a href="https://play.google.com/store/apps/details?id=com.Sailfish.PandaRun&feature=search_result#?t=W251bGwsMSwxLDEsImNvbS5TYWlsZmlzaC5QYW5kYVJ1biJd">Panda Run on Android Market</a>\r\n<br /><br />\r\nUntil next time, remember to support developers!', '2012-04-14 17:49:33');
INSERT INTO `paragraphs` VALUES(655, 299, 'Have you ever played Dance Dance Revolution arcade? Me neither. But for some reason this was the first thought that came to my mind when i played this game. Pet Dash is brought to you by <a href="http://bestcoolfungames.com/bcfg/games/index.php" target="_blank">Best, Cool & Fun Games - Free Game Creation S.A.</a>, who have quite the reputation in Android Market,  Google Play or whatever you want to call it. They are the guys who brought you <a href="https://play.google.com/store/apps/details?id=com.bestcoolfungames.antsmasher#?t=W251bGwsMSwxLDIxMiwiY29tLmJlc3Rjb29sZnVuZ2FtZXMuYW50c21hc2hlciJd" target="_blank">Ant Smasher</a> which is such a cool game that frogs and iguanas like playing, <a href="https://play.google.com/store/apps/details?id=com.bestcoolfungamesfreegameappcreation.bunnyshooter#?t=W251bGwsMSwxLDIxMiwiY29tLmJlc3Rjb29sZnVuZ2FtZXNmcmVlZ2FtZWFwcGNyZWF0aW9uLmJ1bm55c2hvb3RlciJd" target="_blank">Bunny shooter</a> which has been a top free game since the beginning of time, and many other cool titles that have been played countless hours. \r\n<br /><br />\r\nLively colors, cute, funny characters and simplicity are believed to be one of the ingredients for success in a mobile game. Yes, Pet Dash has all of them and not only! If you have young kids, you can easily keep them occupied for quite some time; the game features bunnies and kittens who have to race each other. Here comes the best part and my association with the mentioned arcade: in order to speed up you have to tap or swipe in different directions that match the arrows displayed on the screen - this really makes it a stimulating and addictive game for the child in you. \r\n<br /><br />\r\nThat''s not all! According to the menu, the developers will implement a multiplayer feature, which is probably gonna take this game off the charts in the Google Play store. It will be AWESOME! How couldn''t it? It even has the freakin'' Nyan Cat!\r\n<br /><br />\r\nSo there you have it: bunnies, kittens, cuteness, colorful environment, addictive and stimulating gameplay, and soon to be multiplayer! Definitely give it a try.\r\n<br />\r\n<a href="https://play.google.com/store/apps/details?id=com.bestcoolfungamesfreegamecreation.petdash#?t=W251bGwsMSwxLDIxMiwiY29tLmJlc3Rjb29sZnVuZ2FtZXNmcmVlZ2FtZWNyZWF0aW9uLnBldGRhc2giXQ.." target="_blank">Pet Dash on Google Play</a>\r\n<br /><br />\r\nUntil next time, remember to support developers!', '2012-04-17 08:14:42');
INSERT INTO `paragraphs` VALUES(656, 300, 'I believe Google Inc is probably the most awesome tech company, yet many of its competitors are are trying to mess around with them by playing a dirty game. So why is everybody fucking around with Google? I am no business analyst or whatever those people are called, but bear with me while trying to figure it out.\r\n<br /><br />\r\nGoogle like many other companies: Apple, Oracle, IBM and Microsoft, are playing a huge role in shaping the world, tech-wise. Google is constantly innovating, their products and services are amongst the best on the market, and even better are offered for free. <a href="http://en.wikipedia.org/wiki/Don''t_be_evil" target="_blank">"Don''t be evil"</a> is their unofficial motto, and it seems like others are taking advantage of it and the cult it inspires.\r\n<br /><br />\r\nProbably the most controversial is the open source Android OS, a project which had been started in 2003, long before the market could even think about the iPhone. Then, Google bought Android in 2005, that is 2 years before the first iPhone was unveiled, so to me it doesn''t really sound like Android is an iOS rip-off no matter how you look at it. They just happened to be two projects with almost the same idea and scope. However, after both platforms have been released and became "fully-operational", each of them has copied the other, and if you are an Apple fan boy you are probably raging right now. Since you probably never touched and Android device I will bring it to you now: the notification system since iOS 5, yeah..., that is "inspired" from Android; voice commands have been available on Android since version 2.2 Froyo, does that remind you of anything? Yeah, Siri... Android has "inspired" itself from iOS, but they are not suing left and right for patent infringement. \r\n<br /><br />\r\nDon''t get me wrong, I am no Apple hater or Google fanatic, as a matter of fact I am writing all the articles on this website on a MacBook Air, but to me it does not seem fair that Android and respectively Google are getting punched from all directions because of something that is freakin'' OPEN SOURCE, and they roughly make any money off it, i.e. 550$ mil since the first Android device was launched in 2008; it might seem a lot, but for a company like Google it really isn''t.\r\n<br /><br />\r\nIn addition to all these patent wars, Oracle, a multi-bilion company, is claiming that Google needed a licence to use Java (programming language licensed under under the GNU General Public License) in their <a href="http://en.wikipedia.org/wiki/Android_Open_Source_Project" target="_blank">Android Open Source Project</a>. I am no lawyer, so I don''t really know whether this is right (actually they don''t even know that, the lawsuit is still in progress) but for me, as a software developer and a human being, this does not make any sense whatsoever. Does it mean, that if I use open-source tools and technology to build an application, be it web, mobile, desktop or any other type, and it becomes really popular, Oracle is gonna sue my ass off?\r\n<br /><br />\r\nNow, it''s been Apple, Oracle and we are left to talk about Microsoft. Before I had my first Macintosh powered machine, I had Windows OS and I used to think that buying a MacBook is just not worth the money, maybe because I did not have it, but still... When I started experiencing other products than Microsoft, the opinion I had, rapidly started to change. <br />\r\nMoreover, I had the chance to attend a lectured about software-testing held by one of the test managers at Microsoft Copenhagen, and to my surprise, he said they are still using the waterfall model on some on their projects, and up until 2 years ago they did not even bother to change their approach. For those of you that do not know, the waterfall model is a software development methodology which results in about 75%-80% failure; the projects that succeed are more likely to have lots of bugs. So when we asked the guy why, why did they not make the change sooner, he said they were making enough profit already. Now, I am not saying all their products are buggy, because they do have some really good ones, but this just proves what policy Microsoft has and what they do not value, i.e. their customers. \r\n<br /><br />\r\nWhat made me loathe Microsoft, was the Android-hater marketing campaign they had on twitter. One basically had to smack talk Android in a tweet with #androidhater, or something similar, and the ones that could say most rubbish about Android, would win a Windows Phone. It might sound fun, but this is not how you play the game!\r\n<br /><br />\r\nSo why is Google being slapped from left and right? Maybe because they make a shit tone of money without being evil like everybody else? Dunno. Let me know what you guys think...\r\n<br /><br />\r\nSources: <a href="http://en.wikipedia.org/wiki/Android_Open_Source_Project" target="_blank">Wikipedia</a>, <a href="http://www.businessweek.com/news/2012-04-17/google-says-it-didn-t-need-license-to-use-java-in-android" target="_blank">businessweek.com</a>, <a href="http://www.guardian.co.uk/technology/2012/mar/29/google-earns-more-iphone-android" target="_blank">The Guardian</a>', '2012-04-18 12:34:24');
INSERT INTO `paragraphs` VALUES(657, 301, 'I have to admit: I love this game. And what''s not to be loved about it? It has a funny story behind it, runs well, and has difficult yet not very difficult challenges.\r\n<br />\r\n<br />\r\nSo, the story of this game is that you have to make the monkey fly in order to help it eat. You have to collect a different number of fruits for every level. For example, for a challenge named "Heavenly Fruit" you need to collect 75 fruits to pass the level, 85 fruits to pass the level with 3 starts and 95 to pass it with 3 stars. Of course, the challenge might also be passing ALL levels with ALL stars. Mission impossible for me, but it''s worth a try.\r\n<br />\r\n<br />\r\nYou have elephants and clouds to help you jump and collect fruits. Just be aware, you can only jump on the clouds once, because after you jump they''ll disappear. All of that being said watch out for the rocks and mud pools and enjoy this awesome game. \r\n<br />\r\n<br />\r\nMonkey Flight is one of my all-time favorite games on iOS.  It is a fun, simple, entertaining game which I highly recommend.\r\n', '2012-04-19 02:58:05');
INSERT INTO `paragraphs` VALUES(658, 293, '<b>Update:</b> BENGIGI finally made a "shop" section for Noogra Nuts on iOS. You can chose between several hats. Some of them have special powers, my favorite one is "The Chef" which causes nuts to immediately crack open.', '2012-04-18 12:15:18');
INSERT INTO `paragraphs` VALUES(660, 302, 'Another app review website? NO!<br />\r\nThere are plenty websites that review mobile apps and games, and they do a really good job, so why bother creating another one? We didn''t.\r\nShiftedapps.com is a place where we express our personal opinions about iOS and Android apps and game. \r\n<br /><br />\r\nWe are constantly looking for cool titles on mobile app stores in order to help you discover some must-have games and apps. ', '2012-04-18 06:11:13');
INSERT INTO `paragraphs` VALUES(661, 303, 'With the release of Android 4.0, Google is trying to offer the user a more pleasant and consistent experience for users, with its new <a href="http://developer.android.com/design/index.html" target="_blank">design language</a> named "Holo". This approach combined with the fact that Ice Cream Sandwich can run both on tablets and phones can only leave the user with pleasant results.\r\n<br />\r\n<br />\r\nNow, after almost six months since the first device with Ice Cream Sandwich was publicly available new devices start to see the light of day. So, how can we find the apps that have switched to the new design language.\r\n<br />\r\n<br />\r\nIn the next lines we''ll try to put together a list of apps that are using the "Holo" theme (excluding the ones made by Google):\r\n<br />\r\n<br />\r\n<ul class="unordered_list">\r\n<li>\r\n<img src="https://play.google.com/static/client/images/1282043220-favicon.ico">\r\n<a href="http://bit.ly/IhuCEE" target="_blank"> Box</a>\r\n</li>\r\n<li>\r\n<img src="https://play.google.com/static/client/images/1282043220-favicon.ico">\r\n<a href="http://bit.ly/IhuyVm" target="_blank"> Dropbox</a>\r\n</li>\r\n<li>\r\n<img src="https://play.google.com/static/client/images/1282043220-favicon.ico">\r\n<a href="http://bit.ly/IhuBk0" target="_blank"> Facebook Messenger</a>\r\n</li>\r\n<li>\r\n<img src="https://play.google.com/static/client/images/1282043220-favicon.ico">\r\n<a href="http://bit.ly/IhuzIW" target="_blank"> Imo (instant messenger client)</a>\r\n</li>\r\n<li>\r\n<img src="https://play.google.com/static/client/images/1282043220-favicon.ico">\r\n<a href="http://bit.ly/Ihuwgn" target="_blank"> MX Player</a>\r\n</li>\r\n<li>\r\n<a href="http://spoti.fi/JnkgE0" target="_blank"> Spotify</a>\r\n</li>\r\n<li>\r\n<img src="https://play.google.com/static/client/images/1282043220-favicon.ico">\r\n<a href="http://bit.ly/IhusNn" target="_blank"> Pocket</a>\r\n</li>\r\n<li>\r\n<img src="https://play.google.com/static/client/images/1282043220-favicon.ico">\r\n<a href="http://bit.ly/IhuoNK" target="_blank"> Permission Aware</a>\r\n</li>\r\n<li>\r\n<img src="https://play.google.com/static/client/images/1282043220-favicon.ico">\r\n<a href="http://bit.ly/IhuiWn" target="_blank"> Imdb</a>\r\n</li>\r\n<li>\r\n<img src="https://play.google.com/static/client/images/1282043220-favicon.ico">\r\n<a href="http://bit.ly/Ihui8L" target="_blank"> SyPressure</a>\r\n</li>\r\n<li>\r\n<img src="https://play.google.com/static/client/images/1282043220-favicon.ico">\r\n<a href="http://bit.ly/IhugxD" target="_blank"> QuickPic</a>\r\n</li>\r\n<li>\r\n<img src="https://play.google.com/static/client/images/1282043220-favicon.ico">\r\n<a href="http://bit.ly/Ihuc0Q" target="_blank"> Nova Launcher</a>\r\n</li>\r\n<li>\r\n<img src="https://play.google.com/static/client/images/1282043220-favicon.ico">\r\n<a href="http://bit.ly/I08gJF" target="_blank"> Apex Launcher</a>\r\n</li>\r\n<li>\r\n<img src="https://play.google.com/static/client/images/1282043220-favicon.ico">\r\n<a href="http://bit.ly/Ihu95t" target="_blank"> Boid</a>\r\n</li>\r\n<li>\r\n<img src="https://play.google.com/static/client/images/1282043220-favicon.ico">\r\n<a href="http://bit.ly/IhufKb" target="_blank"> Zinio</a>\r\n</li>\r\n<li>\r\n<img src="https://play.google.com/static/client/images/1282043220-favicon.ico">\r\n<a href="http://bit.ly/IhtVet" target="_blank"> Audio Manager</a>\r\n</li>\r\n<li>\r\n<img src="https://play.google.com/static/client/images/1282043220-favicon.ico">\r\n<a href="http://bit.ly/IhtUHm" target="_blank"> Maildroid</a>\r\n</li>\r\n<li>\r\n<img src="https://play.google.com/static/client/images/1282043220-favicon.ico">\r\n<a href="http://bit.ly/IhtVLA" target="_blank"> SeriesGuide Show Manager</a>\r\n</li>\r\n<li>\r\n<img src="https://play.google.com/static/client/images/1282043220-favicon.ico">\r\n<a href="http://bit.ly/I0f89E" target="_blank"> Pulse</a>\r\n</li>\r\n<li>\r\n<img src="https://play.google.com/static/client/images/1282043220-favicon.ico">\r\n<a href="http://bit.ly/IhtQHE" target="_blank"> Icy time</a>\r\n</li>\r\n<li>\r\n<img src="https://play.google.com/static/client/images/1282043220-favicon.ico">\r\n<a href="http://bit.ly/IhtK2U" target="_blank"> Papermill</a>\r\n</li>\r\n<li>\r\n<img src="https://play.google.com/static/client/images/1282043220-favicon.ico">\r\n<a href="http://bit.ly/IhtEbs" target="_blank"> XDA Premium HD</a>\r\n</li>\r\n<li>\r\n<img src="https://play.google.com/static/client/images/1282043220-favicon.ico">\r\n<a href="http://bit.ly/I0eglz" target="_blank"> Formulae</a>\r\n</li>\r\n<li>\r\n<img src="https://play.google.com/static/client/images/1282043220-favicon.ico">\r\n<a href="http://bit.ly/JpFCgL" target="_blank"> Friendcaster</a>\r\n</li>\r\n<li>\r\n<img src="https://play.google.com/static/client/images/1282043220-favicon.ico">\r\n<a href="http://bit.ly/JpG91Y" target="_blank"> Set CPU for root users</a>\r\n</li>\r\n<li>\r\n<img src="https://play.google.com/static/client/images/1282043220-favicon.ico">\r\n<a href="http://bit.ly/I0eitG" target="_blank"> Minimal Reader</a>\r\n</li>\r\n<li>\r\n<img src="https://play.google.com/static/client/images/1282043220-favicon.ico">\r\n<a href="http://bit.ly/I0enNQ" target="_blank"> myDIaler</a>\r\n</li>\r\n<li>\r\n<img src="https://play.google.com/static/client/images/1282043220-favicon.ico">\r\n<a href="http://bit.ly/I0et8n" target="_blank"> Calculations</a>\r\n</li>\r\n<li>\r\n<img src="https://play.google.com/static/client/images/1282043220-favicon.ico">\r\n<a href="http://bit.ly/I0ex7Z" target="_blank"> TogglePanel</a>\r\n</li>\r\n<li>\r\n<img src="https://play.google.com/static/client/images/1282043220-favicon.ico">\r\n<a href="http://bit.ly/IJEPrU" target="_blank"> MoreQuicklyPanel</a>\r\n</li>\r\n<li>\r\n<img src="https://play.google.com/static/client/images/1282043220-favicon.ico">\r\n<a href="http://bit.ly/JnkomT" target="_blank"> GigBeat</a>\r\n</li>\r\n<li>\r\n<img src="https://play.google.com/static/client/images/1282043220-favicon.ico">\r\n<a href="http://bit.ly/I0eFo7" target="_blank"> Net-A-Porter</a>\r\n</li>\r\n<li>\r\n<img src="https://play.google.com/static/client/images/1282043220-favicon.ico">\r\n<a href="http://bit.ly/I0eKYV" target="_blank"> TED</a>\r\n</li>\r\n<li>\r\n<img src="https://play.google.com/static/client/images/1282043220-favicon.ico">\r\n<a href="http://bit.ly/I0eWXZ" target="_blank"> Bump</a>\r\n</li>\r\n<li>\r\n<img src="https://play.google.com/static/client/images/1282043220-favicon.ico">\r\n<a href="http://bit.ly/I0f1uP" target="_blank"> Catch</a>\r\n</li>\r\n<li>\r\n<img src="https://play.google.com/static/client/images/1282043220-favicon.ico">\r\n<a href="http://bit.ly/I0frBo" target="_blank"> Feedly</a>\r\n</li>\r\n<li>\r\n<img src="https://play.google.com/static/client/images/1282043220-favicon.ico">\r\n<a href="http://bit.ly/I0fvRF" target="_blank"> Pay with square</a>\r\n</li>\r\n<li>\r\n<img src="https://play.google.com/static/client/images/1282043220-favicon.ico">\r\n<a href="http://bit.ly/I0feyd" target="_blank"> Path</a>\r\n</li>\r\n<li>\r\n<img src="https://play.google.com/static/client/images/1282043220-favicon.ico">\r\n<a href="http://bit.ly/I2jSZY" target="_blank"> Hipmunk</a>\r\n</li>\r\n<li>\r\n<img src="https://play.google.com/static/client/images/1282043220-favicon.ico">\r\n<a href="http://bit.ly/IJEghG" target="_blank"> Urban Dictionary</a>\r\n</li>\r\n<li>\r\n<img src="https://play.google.com/static/client/images/1282043220-favicon.ico">\r\n<a href="http://bit.ly/IJEAgf" target="_blank"> Album Art Grabber</a>\r\n</li>\r\n<li>\r\n<img src="https://play.google.com/static/client/images/1282043220-favicon.ico">\r\n<a href="http://bit.ly/IJEHZm" target="_blank"> Colors</a>\r\n</li>\r\n<li>\r\n<img src="https://play.google.com/static/client/images/1282043220-favicon.ico">\r\n<a href="http://bit.ly/HVjsDD" target="_blank"> Robot bird</a>\r\n</li>\r\n<li>\r\n<img src="https://play.google.com/static/client/images/1282043220-favicon.ico">\r\n<a href="http://bit.ly/HVjw6y" target="_blank"> SMS Popup</a>\r\n</li>\r\n</ul>\r\nThe list is a work in progress, so as soon as we find more, the list will be updated. \r\n<br />\r\nAlso we will take a closer look at some of this up and let you know what we think...\r\n<br />\r\n<br />\r\nSources: <a href="http://android-developers.blogspot.com/2012/01/holo-everywhere.html" target="_blank">Android Developer</a>,\r\n<a href="https://play.google.com/store" target="_blank"> Google Play</a>', '2012-04-25 03:51:09');
INSERT INTO `paragraphs` VALUES(662, 304, 'desc desc desc', '2012-04-22 12:25:17');
INSERT INTO `paragraphs` VALUES(663, 305, '<br />\r\n8bit Ninja is a really fun, retro game featuring, you guessed it..a ninja. The main  purpose of the game is to avoid the bouncing fruits coming from the side of the screen. You avoid them by using the arrows on the screen. You can also choose to tilt the screen side to side to make the ninja move, but for me the touch option works better.\r\n<br />\r\n<br />\r\nSo far the game-play doesn''t seem so interesting, but the cool part about this game is that you can customize almost everything  starting from the ninja, the arena and the powerups you''re using.\r\n<br />\r\n<br />\r\nWhen you play the game,coins will fall at random moments or when you destroy the fruits, and sometimes, you will even get the very rare dragon eggs. Accumulating coins in pretty easy, so you can buy power ups. But if you want to change the arena or the player you will need some dragon eggs, which as I said before are pretty hard to get. Fortunately, you have different options for this: you can buy them with real money, earn them by liking their Facebook page or follow them on twitter; another option is to exchange 1000 coins for 1 dragon egg.\r\n<br />\r\n<br />\r\n\r\nAnyways, 8bit Ninja is free on both iOS and Android and I personally think it''s a great little game. You can only play it to kill some time while waiting for the bus or you can really get involved and unlock all the players and arenas.\r\n<br />\r\n<br />', '2012-04-26 12:23:07');
INSERT INTO `paragraphs` VALUES(664, 280, 'Badea Mihai (@MihaiAlinBadea) should write ...', '2012-10-21 12:18:23');

-- --------------------------------------------------------

--
-- Table structure for table `pics`
--

DROP TABLE IF EXISTS `pics`;
CREATE TABLE IF NOT EXISTS `pics` (
  `pic_id` int(11) NOT NULL auto_increment,
  `article_id` int(11) NOT NULL,
  `path` longtext NOT NULL,
  `comments` varchar(400) default NULL,
  `added` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  `thumbnail` tinyint(1) NOT NULL default '0',
  PRIMARY KEY  (`pic_id`),
  KEY `article_id` (`article_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=435 ;

--
-- Dumping data for table `pics`
--

INSERT INTO `pics` VALUES(213, 36, '/picture_uploads/cow_copter.png', '', '2012-03-14 12:47:10', 1);
INSERT INTO `pics` VALUES(385, 294, '/picture_uploads/hungergameiosgame.PNG', 'Katniss Everdeen in the ios game girl on fire', '2012-03-31 08:02:03', 0);
INSERT INTO `pics` VALUES(386, 294, '/picture_uploads/hungergamemockingjaygame.PNG', 'The rebellion symbol : the Mokingjay, in the ios game.', '2012-04-02 08:00:51', 0);
INSERT INTO `pics` VALUES(387, 294, '/picture_uploads/hungergamesgirlonfire.PNG', 'Katniss Everdeeen runs in the woods beyond district 12 ', '2012-04-02 08:00:51', 1);
INSERT INTO `pics` VALUES(388, 293, '/picture_uploads/photo1.jpg', 'Noogra Nuts - starting.', '2012-04-09 08:07:45', 0);
INSERT INTO `pics` VALUES(390, 293, '/picture_uploads/photo3.jpg', 'Noogra Nuts - Jungle mode, jumping over the big nut.', '2012-04-11 05:04:55', 0);
INSERT INTO `pics` VALUES(391, 293, '/picture_uploads/photo4.jpg', 'Noogra Nuts - Survival mode.', '2012-04-09 08:07:45', 0);
INSERT INTO `pics` VALUES(392, 293, '/picture_uploads/photo.jpg', 'Noogra Nuts -Classic', '2012-04-09 08:07:45', 0);
INSERT INTO `pics` VALUES(394, 297, '/picture_uploads/ShaneReaction2.png', 'Shane Reaction nice and fun Android game', '2012-04-11 10:59:46', 1);
INSERT INTO `pics` VALUES(401, 294, '/picture_uploads/thehungergamesgirlongireiosgame.jpg', '', '2012-04-11 06:27:04', 1);
INSERT INTO `pics` VALUES(402, 293, '/picture_uploads/noogranutsiosgame.jpg', 'Noogra Nuts - Jungle mode', '2012-04-11 06:30:48', 1);
INSERT INTO `pics` VALUES(403, 297, '/picture_uploads/shanereactionandroidgame.jpg', 'main menu', '2012-04-11 10:59:46', 0);
INSERT INTO `pics` VALUES(404, 297, '/picture_uploads/shanereactionandroidgame2.jpg', 'shane in mane manu', '2012-04-11 10:59:46', 0);
INSERT INTO `pics` VALUES(405, 297, '/picture_uploads/shanereactionandroidgame3.jpg', 'zombies trying to rape shane', '2012-04-11 10:59:46', 0);
INSERT INTO `pics` VALUES(406, 297, '/picture_uploads/shanereactionandroidgame4.jpg', 'Shane Reaction: epic fail', '2012-04-11 10:59:46', 0);
INSERT INTO `pics` VALUES(407, 297, '/picture_uploads/shanereactionandroidgame5.jpg', 'skills', '2012-04-11 10:59:46', 0);
INSERT INTO `pics` VALUES(408, 297, '/picture_uploads/shanereactionandroidgame6.jpg', 'Shane clears all the zombies like a bosss!!', '2012-04-11 10:59:46', 0);
INSERT INTO `pics` VALUES(410, 298, '/picture_uploads/pandarun1.jpg', 'panda run on android', '2012-04-14 13:10:55', 0);
INSERT INTO `pics` VALUES(411, 298, '/picture_uploads/pandarun2.jpg', 'panda run', '2012-04-14 13:10:55', 0);
INSERT INTO `pics` VALUES(412, 298, '/picture_uploads/pandarun3.png', 'panda runon android', '2012-04-14 13:10:55', 0);
INSERT INTO `pics` VALUES(413, 298, '/picture_uploads/pandarun4.png', 'panda run', '2012-04-14 13:10:55', 0);
INSERT INTO `pics` VALUES(414, 298, '/picture_uploads/templerunandroid.jpg', 'temple run', '2012-04-14 13:10:55', 0);
INSERT INTO `pics` VALUES(415, 298, '/picture_uploads/templerunandroid2.jpg', 'temple run', '2012-04-14 13:10:55', 0);
INSERT INTO `pics` VALUES(416, 298, '/picture_uploads/templerunandroid3.jpg', 'temple run on android', '2012-04-14 13:10:55', 0);
INSERT INTO `pics` VALUES(417, 298, '/picture_uploads/pandaruntemplerunandroid2.jpg', 'panda run a clone of temple run on android', '2012-04-14 17:37:50', 1);
INSERT INTO `pics` VALUES(418, 299, '/picture_uploads/petdash5.jpg', 'multiplayer soon to be - Per Dash Android Game', '2012-04-17 08:36:54', 0);
INSERT INTO `pics` VALUES(419, 299, '/picture_uploads/petdash4.jpg', 'Nyan Cat on Per Dash Android Game', '2012-04-17 08:36:54', 0);
INSERT INTO `pics` VALUES(420, 299, '/picture_uploads/petdash3.jpg', 'Per Dash Android Game', '2012-04-17 08:36:54', 0);
INSERT INTO `pics` VALUES(421, 299, '/picture_uploads/petdash2.jpg', 'Per Dash Android Game', '2012-04-17 08:36:54', 0);
INSERT INTO `pics` VALUES(422, 299, '/picture_uploads/petdash1.jpg', 'notice multiplayer mode for Per Dash Android Game', '2012-04-17 08:36:54', 0);
INSERT INTO `pics` VALUES(423, 299, '/picture_uploads/petdashthumbnail.jpg', 'Per Dash Android Game main menu', '2012-04-17 08:36:54', 1);
INSERT INTO `pics` VALUES(424, 301, '/picture_uploads/monkeyflightfruits1.jpg', 'Monkey Flight - collecting fruits', '2012-04-18 04:28:31', 0);
INSERT INTO `pics` VALUES(425, 301, '/picture_uploads/monkeyflightgame1.jpg', 'Monkey Flight - in the air', '2012-04-18 04:28:31', 0);
INSERT INTO `pics` VALUES(426, 301, '/picture_uploads/Monkeyflightstart1.jpg', 'Monkey Flight -game review', '2012-04-18 04:28:31', 0);
INSERT INTO `pics` VALUES(428, 301, '/picture_uploads/monkeyflightiosgame.jpg', 'Jump on the elephant to make the monkey fly', '2012-04-18 09:38:02', 1);
INSERT INTO `pics` VALUES(429, 303, '/picture_uploads/building_blocks_landing.png', '', '2012-04-22 11:11:52', 1);
INSERT INTO `pics` VALUES(430, 305, '/picture_uploads/8bitNinjareview.jpg', '', '2012-04-26 10:00:54', 0);
INSERT INTO `pics` VALUES(431, 305, '/picture_uploads/Bankat8bitninja.jpg', '', '2012-04-26 10:00:54', 0);
INSERT INTO `pics` VALUES(432, 305, '/picture_uploads/Catchingthedragonegg.jpg', '', '2012-04-26 10:00:54', 0);
INSERT INTO `pics` VALUES(433, 305, '/picture_uploads/Fruitsfalling.jpg', 'Review for 8bitNinja\r\n', '2012-04-26 10:00:54', 1);
INSERT INTO `pics` VALUES(434, 305, '/picture_uploads/Powerupsstore.jpg', '', '2012-04-26 10:00:54', 0);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `user_id` int(11) NOT NULL auto_increment,
  `name` varchar(30) default NULL,
  `description` varchar(300) default NULL,
  `pic` varchar(300) default NULL,
  PRIMARY KEY  (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` VALUES(1, 'default', 'default_user', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `vids`
--

DROP TABLE IF EXISTS `vids`;
CREATE TABLE IF NOT EXISTS `vids` (
  `vid_id` int(11) NOT NULL auto_increment,
  `article_id` int(11) NOT NULL,
  `link` varchar(300) NOT NULL,
  `comments` varchar(300) default NULL,
  PRIMARY KEY  (`vid_id`),
  KEY `article_id` (`article_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=205 ;

--
-- Dumping data for table `vids`
--

INSERT INTO `vids` VALUES(7, 36, '', '');
INSERT INTO `vids` VALUES(185, 280, '', '');
INSERT INTO `vids` VALUES(186, 281, '', '');
INSERT INTO `vids` VALUES(192, 293, 'http://youtu.be/d1Kiv-paWyM', '');
INSERT INTO `vids` VALUES(193, 294, 'http://youtu.be/9tNA8MmrD5s', '');
INSERT INTO `vids` VALUES(196, 297, '', '');
INSERT INTO `vids` VALUES(197, 298, '', '');
INSERT INTO `vids` VALUES(198, 299, 'RLYrb1gc1NE', '');
INSERT INTO `vids` VALUES(199, 300, '', '');
INSERT INTO `vids` VALUES(200, 301, '', '');
INSERT INTO `vids` VALUES(201, 302, '', '');
INSERT INTO `vids` VALUES(202, 303, '', '');
INSERT INTO `vids` VALUES(203, 304, '', '');
INSERT INTO `vids` VALUES(204, 305, 'http://youtu.be/PrGOa_GZtME', '8bit Ninja Debut Trailer - Game review');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `articles_users`
--
ALTER TABLE `articles_users`
  ADD CONSTRAINT `articles_users_ibfk_1` FOREIGN KEY (`article_id`) REFERENCES `articles` (`article_id`),
  ADD CONSTRAINT `articles_users_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`);

--
-- Constraints for table `categories`
--
ALTER TABLE `categories`
  ADD CONSTRAINT `categories_ibfk_1` FOREIGN KEY (`parent_category`) REFERENCES `categories` (`category_id`);

--
-- Constraints for table `categories_articles`
--
ALTER TABLE `categories_articles`
  ADD CONSTRAINT `categories_articles_ibfk_1` FOREIGN KEY (`article_id`) REFERENCES `articles` (`article_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `categories_articles_ibfk_2` FOREIGN KEY (`category_id`) REFERENCES `categories` (`category_id`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Constraints for table `credentials`
--
ALTER TABLE `credentials`
  ADD CONSTRAINT `credentials_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `logins`
--
ALTER TABLE `logins`
  ADD CONSTRAINT `logins_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `paragraphs`
--
ALTER TABLE `paragraphs`
  ADD CONSTRAINT `paragraphs_ibfk_1` FOREIGN KEY (`article_id`) REFERENCES `articles` (`article_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `pics`
--
ALTER TABLE `pics`
  ADD CONSTRAINT `pics_ibfk_1` FOREIGN KEY (`article_id`) REFERENCES `articles` (`article_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `vids`
--
ALTER TABLE `vids`
  ADD CONSTRAINT `vids_ibfk_1` FOREIGN KEY (`article_id`) REFERENCES `articles` (`article_id`) ON DELETE CASCADE ON UPDATE CASCADE;
