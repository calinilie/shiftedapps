<?php
session_start();
ob_start();
$path=dirname(__FILE__);
require_once $path.'/Commands/adminCommands.php';
require_once $path.'/Blocks/Block.php';
require_once $path.'/Logic/AdminController.php';?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <style>
        .alert{
            color: red;
            font-weight: bold;
        }
    </style>
</head>
<body>
<?php   
    if (isset ($_GET['message'])) echo '<p style="text-align:center; font-size:30px; color:green">success, i hope...</p>';

    $commands=AdminCommands::getInstance();
    if (AdminController::getInstance()->isLoggedIn()==false){
        if (count($_POST)>0){
            $data = array();
            foreach ($_POST as $name => $value){
                $data[$name]=$value;

            }
            $commands->executeCommand("do_admin_auth", $data);
        }
        else $commands->executeCommand("admin_auth", NULL);
    }
    else{
        
        if (isset ($_GET['cmd'])){
            $cmd=$_GET['cmd'];
        }
        else{
            if (isset ($_POST['cmd'])) $cmd=$_POST['cmd'];
            else $cmd="";
        }

        $data = array();
        if (isset ($_POST['cmd'])){
            foreach ($_POST as $name => $value){
                $data[$name]=$value;
            }
        }
        else{
             foreach ($_GET as $name => $value){
                $data[$name]=$value;
            }
        }
        

        switch ($cmd){
            case 'insert_article':
                $commands->executeCommand($cmd, NULL);
                break;
            case 'do_insert_article':
                $commands->executeCommand($cmd, $data);
                break;
            case 'update_article':
                $commands->executeCommand($cmd, $data);
                break;
            case 'do_update_article':
                $commands->executeCommand($cmd, $data);
                break;
            case 'list':
                echo 'list';
                break;
            case 'logout':
                $commands->executeCommand($cmd, NULL);
                break;
            default:
                Block::renderBlock(Helper::getServerName()."/FrontEndAdmin/list.php", NULL);
                break;
        }
       
    }
?>
<script type="text/javascript">
    function validateArticle(){
        var alertMsg=$('<div class="alert">you need to fill this out</div>');
        var title=$('input[name="article_title"]');
        if (title.val()=='') {
            alertMsg.insertBefore(title.parent().find('.clear'));
            title.parent().find('label').css("color",'red');
            return false;
        }
        else {
            title.parent().find('.alert').remove();
            title.parent().find('label').css("color",'black');
        }
        
        var categories=$('.categories');
        if (!categories.find('input[name=article_category]:checked').val()){
            categories.find('label').css("color", "red");
            alertMsg.appendTo(categories);
            return false;
        }
        else{
            categories.find('label').css("color", "black");
            categories.find('.alert').remove();
        }
        
        var desc = $('textarea[name="article_description"]');
        if (desc.val()==''){
            alertMsg.insertBefore(desc.parent().find('.clear'));
            desc.parent().find('label').css("color",'red');
            return false;
        }
        else{
            desc.parent().find('.alert').remove();
            desc.parent().find('label').css("color",'black');
        }
        
        var alertParagraph=$('<div class="alert">Either write something or remove this paragraph -></div>');
        var ok=true;
        $('.paragraph_text').each(function(){
           if ($(this).val()==''){
               $('<div class="alert">Either write something or remove this paragraph       ------->>> </div>').appendTo($(this).parent())
               ok=false;
           }
           else {
               $(this).parent().find('.alert').remove();
           }
        });
        
        var featured=$('input[name=article_featured]');
        if (featured.is(":checked")){
            if (!$('.uploaded_picture').html()){
                featured.parent().find('label').css("color", "red");
                $('<div class="alert">You should upload a picture and set it as thumbnail if you want this article to be featured</div>').insertBefore(featured.parent().find('.clear'));
                return false;
            }
            else {
                featured.parent().find('label').css("color", "black");
                featured.parent().find('.alert').remove();
            }
            if (!$('input[name="pic_thumbnail"]:checked').val()){
                $('input[name="pic_thumbnail"]').parent().find('label').css("color", "red");
                $('<div class="alert">Assign one of the pictures as thumbnail</div>').insertAfter($('#pictures').find('h3'));
                return false;
            }
            else{
                $('input[name="pic_thumbnail"]:checked').parent().find('label').css("color", "black");
                $('#pictures').find('.alert').remove();
            }
        }
        return true && ok;
    }
</script>
</body>
</html>
